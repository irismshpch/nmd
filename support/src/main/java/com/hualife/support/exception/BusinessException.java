/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月18日
 *******************************************************************************/


package com.hualife.support.exception;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class BusinessException extends RuntimeException {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = -36524635072903619L;

	public BusinessException(String message){
		super(message);
	}
	
	public BusinessException(String message , Throwable cause){
		super(message , cause);
	}

}

/*
 * 修改历史
 * $Log$ 
 */