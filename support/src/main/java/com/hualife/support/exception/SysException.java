/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月25日
 *******************************************************************************/


package com.hualife.support.exception;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class SysException extends RuntimeException {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = -1229237722512234578L;

	public SysException(){
		super("系统错误");
	}
	
	public SysException(String message){
		super(message);
	}
	
	public SysException(String message , Throwable cause){
		super(message , cause);
	}
}

/*
 * 修改历史
 * $Log$ 
 */