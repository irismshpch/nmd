/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月25日
 *******************************************************************************/

package com.hualife.support.util;

import java.io.IOException;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.hualife.support.exception.SysException;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class JsonUtil {
	
	private static ObjectMapper jacksonMapper = new ObjectMapper();
	/**
	 * 蛇形json转为驼峰bean
	 * @param jsonStr
	 * @param cl
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static Object SnakeJson2CamelBean(String jsonStr, Class<?> cl)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				false); // 主要目的是为了防止由于接口变动出现多余字段时转换报错
		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE); // 需进行蛇形转驼峰处理
		return mapper.readValue(jsonStr, cl);
	}

	/**
	 * 驼峰bean转为蛇形json
	 * @param ts
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static JSONObject CamelBean2SnakeJson(Object ts)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		String data = mapper.writeValueAsString(ts);
		return JSONObject.parseObject(data);  
	}
	
	public static String objectToJackson (Object object) {
		try {
			if (jacksonMapper == null) {
				jacksonMapper = new ObjectMapper();
			}
			return jacksonMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 把Json转成Map
	 * @param json  JSON字符串
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Map jackson2Map(String json){
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mapper.readValue(json, Map.class);
		} catch (JsonParseException e) {
			throw new SysException("把Json转成Map失败");
		} catch (JsonMappingException e) {
			throw new SysException("把Json转成Map失败");
		} catch (IOException e) {
			throw new SysException("把Json转成Map失败");
		}
	}
	
}

/*
 * 修改历史 $Log$
 */