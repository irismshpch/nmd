/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年5月28日
 *******************************************************************************/


package com.hualife.support.util;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class PageDTO {
	private static final int PageQuerySize = 50;
	private int pageIndex = 0; // 当前页号
	private int pageSize = PageQuerySize; // 每页显示的行数
	private int startRow = 0; // 当前页在数据库中的起始行
	private int endRow; // 当前页在数据库中的截止行
	private Object parameters; // 查询参数
	
	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStartRow() {
		startRow = (pageIndex - 1) * pageSize;
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		endRow = pageIndex * pageSize;
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
	
	public Object getParameters() {
		return parameters;
	}

	public void setParameters(Object parameters) {
		this.parameters = parameters;
	}
}

/*
 * 修改历史
 * $Log$ 
 */