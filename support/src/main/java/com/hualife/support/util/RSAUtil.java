/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年2月6日
 *******************************************************************************/

package com.hualife.support.util;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

import sun.misc.BASE64Decoder;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@SuppressWarnings("restriction")
public class RSAUtil {

	/**
	 * 公钥加密
	 * 
	 * @param key
	 *            公钥
	 * @param bt_plaintext
	 *            加密内容
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String key, byte[] bt_plaintext)
			throws Exception {
		PublicKey publicKey = getPublicKey(key);
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		String bt_encrypted = Base64.encodeBase64String(cipher
				.doFinal(bt_plaintext));
		String b = Base64.encodeBase64String(bt_encrypted.getBytes());
		return b;
	}

	/**
	 * 私钥解密
	 * 
	 * @param key
	 *            私钥
	 * @param bt_encrypted
	 *            解密内容
	 * @return
	 * @throws Exception
	 */
	public static byte[] decrypt(String key, byte[] bt_encrypted)
			throws Exception {
		PrivateKey privateKey = getPrivateKey(key);
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] bt_original = cipher.doFinal(bt_encrypted);
		return bt_original;
	}

	/**
	 * 通过传入的字符串key，转换换公钥PublicKey
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private static PublicKey getPublicKey(String key) throws Exception {
		byte[] keyBytes;
		keyBytes = (new BASE64Decoder()).decodeBuffer(key);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(keySpec);
		return publicKey;
	}

	/**
	 * 通过传入的字符串key，转换私钥PrivateKey
	 * 
	 * @param base64
	 *            String to PrivateKey
	 * @throws Exception
	 */
	private static PrivateKey getPrivateKey(String key) throws Exception {
		byte[] keyBytes;
		keyBytes = (new BASE64Decoder()).decodeBuffer(key);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		return privateKey;
	}

	/**
	 * 获取字符串hash值
	 * 
	 * @param key
	 * @return
	 */
	public static int toHash(String key) {
		int arraySize = 11113; // 数组大小一般取质数
		int hashCode = 0;
		for (int i = 0; i < key.length(); i++) { // 从字符串的左边开始计算
			int letterValue = key.charAt(i) - 96;// 将获取到的字符串转换成数字，比如a的码值是97，则97-96=1就代表a的值，同理b=2；
			hashCode = ((hashCode << 5) + letterValue) % arraySize;// 防止编码溢出，对每步结果都进行取模运算
		}
		return hashCode;
	}
}

/*
 * 修改历史 $Log$
 */