/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月19日
 *******************************************************************************/

package com.hualife.support.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @description: MD5-32位
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class MD5Util {

	private static final String CHARSET = "UTF-8";

	public static final String sign(String str) {
		return sign(str, CHARSET);
	}

	/**
	 * @param str
	 *            需要签名的字符串
	 * @param sign
	 *            待比较的MD5字符串
	 * @return 比较结果
	 */
	public static final boolean verify(String str, String sign) {
		return verify(str, sign, CHARSET);
	}

	/**
	 * @param str
	 *            需要签名的字符串
	 * @param sign
	 *            待比较的MD5字符串
	 * @param charset
	 *            编码格式
	 * @return 比较结果
	 */
	public static final boolean verify(String str, String sign, String charset) {
		String mysign = sign(str, charset);
		if (mysign.equals(sign)) {
			return true;
		} else {
			return false;
		}
	}

	public static final String sign(String str, String charset) {
		try {
			String md5Str = DigestUtils.md5Hex(str.getBytes(charset));
			return md5Str;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:"
					+ charset);
		}
	}

	public static String Md5(String plainText) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			try {
				md.update(plainText.getBytes("utf-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString().substring(8, 24).toUpperCase();// 32位的加密
			// System.out.println("result: " + buf.toString().substring(8,
			// 24));// 16位的加密
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] argv) {
		System.out.println(MD5Util.Md5("a"));
	}

	/**
	 * 签名字符串
	 * 
	 * @param text
	 *            需要签名的字符串
	 * @param key
	 *            密钥
	 * @param input_charset
	 *            编码格式
	 * @return 签名结果
	 */
	public static String sign(String text, String key, String input_charset) {
		text = text + key;
		return DigestUtils.md5Hex(getContentBytes(text, input_charset));
	}

	/**
	 * @param content
	 * @param charset
	 * @return
	 * @throws SignatureException
	 * @throws UnsupportedEncodingException
	 */
	private static byte[] getContentBytes(String content, String charset) {
		if (charset == null || "".equals(charset)) {
			return content.getBytes();
		}
		try {
			return content.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:"
					+ charset);
		}
	}

}

/*
 * 修改历史 $Log$
 */