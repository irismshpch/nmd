/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年2月6日
 *******************************************************************************/


package com.hualife.support.util;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class ColorUtil {
	public static String getColor(String key) {
		int hashValue = RSAUtil.toHash(key);
		String bgColor = hashValue + "";
		String prefix = "#";
		int len = bgColor.length();
		if (len < 6) {
			for (int j = 0; j < 6 - len; j++) {
				prefix += "0";
			}
		}
		if (len > 6) {
			bgColor = bgColor.substring(0, 6);
		}
		bgColor = prefix + bgColor;
		return bgColor;
	}
}

/*
 * 修改历史
 * $Log$ 
 */