package com.hualife.md.common.dao.mybatis.mapper;

import com.hualife.md.common.dao.mybatis.entity.DefKv;
import com.hualife.md.common.dao.mybatis.entity.DefKvExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DefKvMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    long countByExample(DefKvExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int deleteByExample(DefKvExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int deleteByPrimaryKey(String k);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int insert(DefKv record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int insertSelective(DefKv record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    List<DefKv> selectByExample(DefKvExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    DefKv selectByPrimaryKey(String k);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int updateByExampleSelective(@Param("record") DefKv record, @Param("example") DefKvExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int updateByExample(@Param("record") DefKv record, @Param("example") DefKvExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int updateByPrimaryKeySelective(DefKv record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table def_kv
     *
     * @mbg.generated Wed Sep 20 17:14:01 CST 2017
     */
    int updateByPrimaryKey(DefKv record);
}