/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年5月7日
 *******************************************************************************/


package com.hualife.md.common.service;

import java.util.List;
import java.util.Map;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IWeChatTagService {
	public List<String> queryTagUser(String tagId, String token);
	
	public List<String> queryTagUser(String[] tags, String token);
	
	public void deleteTagUser(String token, Map<String, Object> paraMap);
	
	public void addTagUser(String token, Map<String, Object> paraMap);
}

/*
 * 修改历史
 * $Log$ 
 */