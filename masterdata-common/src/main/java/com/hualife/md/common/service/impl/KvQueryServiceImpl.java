/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月22日
 *******************************************************************************/

package com.hualife.md.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hualife.md.common.dao.mybatis.entity.DefKv;
import com.hualife.md.common.dao.mybatis.mapper.DefKvMapper;
import com.hualife.md.common.service.IKvQueryService;
import com.hualife.support.exception.SysException;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("kvQueryService")
public class KvQueryServiceImpl implements IKvQueryService {
	@Autowired
	private DefKvMapper defKvMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.common.service.KvQueryService#getValue(java.lang.String)
	 */
	@Override
	public String getValue(String key) {
		if(key == null || "".equals(key.trim()))
			throw new SysException("key值不能为空！");
		DefKv kv = defKvMapper.selectByPrimaryKey(key);
		if(kv == null) 
			throw new SysException("def_kv表中找不到对应的记录，请核实！");
		return kv.getV();
	}

}

/*
 * 修改历史 $Log$
 */