/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年5月9日
 *******************************************************************************/


package com.hualife.md.common.service;

import com.hualife.md.common.dto.wx.user.WeChatUserGetResponseDTO;


/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IWeChatUserService {
	public WeChatUserGetResponseDTO queryUser(String userId, String token);
}

/*
 * 修改历史
 * $Log$ 
 */