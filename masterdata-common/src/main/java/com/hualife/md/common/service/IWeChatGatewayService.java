/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年5月2日
 *******************************************************************************/


package com.hualife.md.common.service;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IWeChatGatewayService {
	public String getToken();
}

/*
 * 修改历史
 * $Log$ 
 */