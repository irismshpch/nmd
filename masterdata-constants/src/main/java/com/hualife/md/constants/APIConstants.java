/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月25日
 *******************************************************************************/


package com.hualife.md.constants;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class APIConstants {
	public static final String RESULT_SUCCESS = "0000";
	public static final String RESULT_FAIL = "0001";
	public static final String RESULT_EXCEPTION = "9999";
	
	public static final String RESULT_CODE = "result_code";
	public static final String RESULT_MESSAGE = "result_message";
	public static final String DATA = "data";
	
	public static final String VERIFY_SUCCESS = "Y";
	public static final String VERIFY_FAIL = "N";
	
}

/*
 * 修改历史
 * $Log$ 
 */