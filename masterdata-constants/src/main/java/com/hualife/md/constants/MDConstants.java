/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/

package com.hualife.md.constants;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class MDConstants {
	public static final String IS_SYNED = "Y";
	public static final String NOT_SYNED = "N";
	public static final String SYNED_ERROR = "E";

	public static final String APPROVAL_SUCCESS = "Y";
	public static final String APPROVAL_FAIL = "N";

	public static final String SEPARATOR_SEMICOLON = ";";
	public static final String SEPARATOR_COMMA = ",";
	// public static final String SEPARATOR_BACKSLANT = "\\";
	public static final String SEPARATOR_SLANT = "/";
	public static final String SEPARATOR_DOT = ".";
	public static final String PREFIX_MANAGECOM = "managecom=";
	public static final String PREFIX_OrganizationUnit = "OU=";
	public static final String ACCOUNT_DISABLE = "2";
	public static final String ACCOUNT_VALID = "0";

	public static final String TOP_ORGANIZATION = "86";
	public static final String TOP_ORGANIZATION_NAME = "华夏保险";

	public static final int SHORT_UNIT_PART = 2;

	public static final String HIERARCHY_CMS = "1"; // 销管层级
	public static final String HIERARCHY_DOMAIN = "2"; // 域控层级

	public static final String MD_USER = "mduser";

}

/*
 * 修改历史 $Log$
 */