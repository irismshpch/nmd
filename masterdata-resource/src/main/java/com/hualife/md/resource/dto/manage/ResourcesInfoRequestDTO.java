/**
 * This class is generated by the generator.
 * Any question, please contact to sunxiaodong(13401146791).
 */

package com.hualife.md.resource.dto.manage;

/**
 * auto generate ResourcesInfoRequestDTO model
 * 
 * @author generator
 * @date 2017-10-23
 */
public class ResourcesInfoRequestDTO {
	private String resourceUrl;
	private String appId;
	private String resourceName;
	private String resourceDescription;

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceDescription() {
		return resourceDescription;
	}

	public void setResourceDescription(String resourceDescription) {
		this.resourceDescription = resourceDescription;
	}

}