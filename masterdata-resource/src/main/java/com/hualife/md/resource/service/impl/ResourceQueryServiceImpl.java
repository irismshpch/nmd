/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月23日
 *******************************************************************************/


package com.hualife.md.resource.service.impl;

import org.springframework.stereotype.Service;

import com.hualife.md.resource.dao.ex.entity.ResourceMemberInfo;
import com.hualife.md.resource.service.IResourceQueryService;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("resourceQueryService")
public class ResourceQueryServiceImpl implements IResourceQueryService {

	/* (non-Javadoc)
	 * @see com.hualife.md.resource.service.IResourceManageService#resourceMemberVerification(com.hualife.md.resource.dao.ex.entity.ResourceMemberInfo)
	 */
	@Override
	public boolean resourceMemberVerification(
			ResourceMemberInfo resourceMemberInfo) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

}

/*
 * 修改历史
 * $Log$ 
 */