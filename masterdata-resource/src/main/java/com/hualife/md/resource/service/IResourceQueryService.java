/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月23日
 *******************************************************************************/


package com.hualife.md.resource.service;

import com.hualife.md.resource.dao.ex.entity.ResourceMemberInfo;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IResourceQueryService {
	
	public boolean resourceMemberVerification(ResourceMemberInfo resourceMemberInfo) throws Exception;
}

/*
 * 修改历史
 * $Log$ 
 */