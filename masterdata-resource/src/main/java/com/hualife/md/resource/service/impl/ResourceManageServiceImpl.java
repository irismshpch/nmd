/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月23日
 *******************************************************************************/

package com.hualife.md.resource.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hualife.md.constants.MDConstants;
import com.hualife.md.resource.dao.mybatis.entity.ResourceApproval;
import com.hualife.md.resource.dao.mybatis.entity.ResourceInfo;
import com.hualife.md.resource.dao.mybatis.entity.ResourceInfoExample;
import com.hualife.md.resource.dao.mybatis.entity.ResourceTeamRela;
import com.hualife.md.resource.dao.mybatis.entity.ResourceTeamRelaExample;
import com.hualife.md.resource.dao.mybatis.mapper.ResourceApprovalMapper;
import com.hualife.md.resource.dao.mybatis.mapper.ResourceInfoMapper;
import com.hualife.md.resource.dao.mybatis.mapper.ResourceTeamRelaMapper;
import com.hualife.md.resource.service.IResourceManageService;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("resourceManageService")
public class ResourceManageServiceImpl implements IResourceManageService {
	@Autowired
	private ResourceInfoMapper resourceInfoMapper;
	@Autowired
	private ResourceApprovalMapper resourceApprovalMapper;
	@Autowired
	private ResourceTeamRelaMapper resourceTeamRelaMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.resource.service.IResourceManageService#resourceAddition
	 * (java.lang.String, java.util.List)
	 */
	@Override
	@Transactional
	public void resourceAddition(String operator,
			List<ResourceInfo> resourceList) throws Exception {
		for (ResourceInfo resourceInfo : resourceList) {
			resourceInfo.setCreateUser(operator);
			resourceInfo.setCreateDate(new Date());
			resourceInfo.setUpdateUser(operator);
			resourceInfo.setUpdateDate(new Date());
			resourceInfoMapper.insertSelective(resourceInfo);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.resource.service.IResourceManageService#resourceRemove
	 * (java.lang.String, java.util.List)
	 */
	@Override
	@Transactional
	public void resourceRemove(String operator, List<ResourceInfo> resourceList)
			throws Exception {
		for (ResourceInfo resourceInfo : resourceList) {
			ResourceInfoExample resourceInfoExample = new ResourceInfoExample();
			ResourceInfoExample.Criteria resourceInfoCriteria = resourceInfoExample
					.createCriteria();
			resourceInfoCriteria.andResourceUrlEqualTo(resourceInfo
					.getResourceUrl());
			resourceInfoCriteria.andAppIdEqualTo(resourceInfo.getAppId());
			List<ResourceInfo> resourceInfoResult = resourceInfoMapper
					.selectByExample(resourceInfoExample);

			ResourceTeamRelaExample resourceTeamRelaExample = new ResourceTeamRelaExample();
			ResourceTeamRelaExample.Criteria resourceTeamRelaExampleCriteria = resourceTeamRelaExample
					.createCriteria();
			resourceTeamRelaExampleCriteria
					.andResourceIdEqualTo(resourceInfoResult.get(0)
							.getResourceId());
			resourceTeamRelaMapper.deleteByExample(resourceTeamRelaExample);

			resourceInfoMapper.deleteByPrimaryKey(resourceInfoResult.get(0)
					.getResourceId());

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.resource.service.IResourceManageService#resourceTeamApply
	 * (com.hualife.md.resource.dao.mybatis.entity.ResourceApproval)
	 */
	@Override
	@Transactional
	public void resourceTeamApply(ResourceApproval resourceApproval)
			throws Exception {
		String operator = resourceApproval.getApplyUser();
		resourceApproval.setCreateUser(operator);
		resourceApproval.setCreateDate(new Date());
		resourceApproval.setUpdateUser(operator);
		resourceApproval.setUpdateDate(new Date());
		resourceApprovalMapper.insertSelective(resourceApproval);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.resource.service.IResourceManageService#resourceTeamApproval
	 * (com.hualife.md.resource.dao.mybatis.entity.ResourceApproval,
	 * java.util.List)
	 */
	@Override
	@Transactional
	public void resourceTeamApproval(ResourceApproval resourceApproval,
			List<ResourceInfo> resourceList) throws Exception {
		String operator = resourceApproval.getApplyUser();
		resourceApproval.setUpdateUser(operator);
		resourceApproval.setUpdateDate(new Date());
		resourceApprovalMapper.updateByPrimaryKeySelective(resourceApproval);

		ResourceApproval resourceApprovalResult = resourceApprovalMapper
				.selectByPrimaryKey(resourceApproval.getApplyId());
		if (MDConstants.APPROVAL_SUCCESS.equals(resourceApproval
				.getApprovalResult().trim())) {
			for (ResourceInfo resourceInfo : resourceList) {
				ResourceInfoExample example = new ResourceInfoExample();
				ResourceInfoExample.Criteria criteria = example
						.createCriteria();
				criteria.andResourceUrlEqualTo(resourceInfo.getResourceUrl());
				criteria.andAppIdEqualTo(resourceApprovalResult.getAppId());
				List<ResourceInfo> resourceInfoResult = resourceInfoMapper
						.selectByExample(example);
				ResourceTeamRela resourceTeamRela = new ResourceTeamRela();
				resourceTeamRela.setResourceId(resourceInfoResult.get(0)
						.getResourceId());
				resourceTeamRela.setTeamId(resourceApprovalResult.getTeamId());
				resourceTeamRela.setCreateUser(operator);
				resourceTeamRela.setCreateDate(new Date());
				resourceTeamRela.setUpdateUser(operator);
				resourceTeamRela.setUpdateDate(new Date());
				resourceTeamRelaMapper.insertSelective(resourceTeamRela);
			}
		}

	}

}

/*
 * 修改历史 $Log$
 */