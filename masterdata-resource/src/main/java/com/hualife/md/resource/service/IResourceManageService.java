/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月23日
 *******************************************************************************/


package com.hualife.md.resource.service;

import java.util.List;

import com.hualife.md.resource.dao.mybatis.entity.ResourceApproval;
import com.hualife.md.resource.dao.mybatis.entity.ResourceInfo;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IResourceManageService {
	
	public void resourceAddition(String operator, List<ResourceInfo> resourceList) throws Exception;
	
	public void resourceRemove(String operator, List<ResourceInfo> resourceList) throws Exception;
	
	public void resourceTeamApply(ResourceApproval resourceApproval) throws Exception;
	
	public void resourceTeamApproval(ResourceApproval resourceApproval, List<ResourceInfo> resourceList) throws Exception;
}

/*
 * 修改历史
 * $Log$ 
 */