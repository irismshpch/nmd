/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月23日
 *******************************************************************************/

package com.hualife.md.resource.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hualife.md.constants.APIConstants;
import com.hualife.md.constants.MDConstants;
import com.hualife.md.resource.dao.ex.entity.ResourceMemberInfo;
import com.hualife.md.resource.dao.mybatis.entity.ResourceApproval;
import com.hualife.md.resource.dao.mybatis.entity.ResourceInfo;
import com.hualife.md.resource.dto.manage.ResourceManageRequestDTO;
import com.hualife.md.resource.dto.manage.ResourceMemberVerificationRequestDTO;
import com.hualife.md.resource.dto.manage.ResourceMemberVerificationResponseDTO;
import com.hualife.md.resource.dto.manage.ResourceTeamApplyRequestDTO;
import com.hualife.md.resource.dto.manage.ResourceTeamApprovalRequestDTO;
import com.hualife.md.resource.dto.manage.ResourcesInfoRequestDTO;
import com.hualife.md.resource.service.IResourceManageService;
import com.hualife.md.resource.service.IResourceQueryService;
import com.hualife.support.exception.BusinessException;
import com.hualife.support.exception.SysException;
import com.hualife.support.util.JsonUtil;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@RestController
@RequestMapping(value = "/api/v1")
public class ResourceController {
	private static final Logger logger = LoggerFactory
			.getLogger(ResourceController.class);

	@Autowired
	private IResourceQueryService resourceQueryService;

	@Autowired
	private IResourceManageService resourceManageService;

	@RequestMapping(value = "/resource/addition", method = RequestMethod.POST)
	public Map<String, Object> resourceAddition(HttpServletRequest request) {
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim())) {
			throw new BusinessException("请求条件不允许为空！");
		}

		try {
			ResourceManageRequestDTO requestDTO = (ResourceManageRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, ResourceManageRequestDTO.class);
			String operator = requestDTO.getOperator();
			List<ResourcesInfoRequestDTO> resourcesInfoList = requestDTO
					.getResourcesInfo();
			if (operator == null || "".equals(operator.trim())) {
				throw new BusinessException("操作人不允许为空！");
			}
			if (resourcesInfoList == null || resourcesInfoList.size() < 1) {
				throw new BusinessException("资源信息不允许为空！");
			}

			List<ResourceInfo> resourceList = new ArrayList<ResourceInfo>();
			for (ResourcesInfoRequestDTO resourcesInfo : resourcesInfoList) {
				if (resourcesInfo.getResourceUrl() == null
						|| "".equals(resourcesInfo.getResourceUrl().trim())) {
					throw new BusinessException("资源URL不允许为空！");
				}
				if (resourcesInfo.getResourceName() == null
						|| "".equals(resourcesInfo.getResourceName().trim())) {
					throw new BusinessException("资源名称不允许为空！");
				}
				if (resourcesInfo.getAppId() == null
						|| "".equals(resourcesInfo.getAppId().trim())) {
					throw new BusinessException("应用编码不允许为空！");
				}
				ResourceInfo resourceInfo = new ResourceInfo();
				resourceInfo.setResourceUrl(resourcesInfo.getResourceUrl()
						.trim());
				resourceInfo.setResourceName(resourcesInfo.getResourceName()
						.trim());
				resourceInfo.setAppId(resourcesInfo.getAppId().trim());
				resourceInfo.setDescription(resourcesInfo
						.getResourceDescription().trim());
				resourceList.add(resourceInfo);
			}
			resourceManageService.resourceAddition(operator.trim(),
					resourceList);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", "{}");
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/addition交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/addition交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/addition交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/addition交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	@RequestMapping(value = "/resource/remove", method = RequestMethod.POST)
	public Map<String, Object> resourceRemove(HttpServletRequest request) {
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim())) {
			throw new BusinessException("请求条件不允许为空！");
		}

		try {
			ResourceManageRequestDTO requestDTO = (ResourceManageRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, ResourceManageRequestDTO.class);
			String operator = requestDTO.getOperator();
			List<ResourcesInfoRequestDTO> resourcesInfoList = requestDTO
					.getResourcesInfo();
			if (operator == null || "".equals(operator.trim())) {
				throw new BusinessException("操作人不允许为空！");
			}
			if (resourcesInfoList == null || resourcesInfoList.size() < 1) {
				throw new BusinessException("资源信息不允许为空！");
			}

			List<ResourceInfo> resourceList = new ArrayList<ResourceInfo>();
			for (ResourcesInfoRequestDTO resourcesInfo : resourcesInfoList) {
				if (resourcesInfo.getResourceUrl() == null
						|| "".equals(resourcesInfo.getResourceUrl().trim())) {
					throw new BusinessException("资源URL不允许为空！");
				}
				if (resourcesInfo.getAppId() == null
						|| "".equals(resourcesInfo.getAppId().trim())) {
					throw new BusinessException("应用编码不允许为空！");
				}
				ResourceInfo resourceInfo = new ResourceInfo();
				resourceInfo.setResourceUrl(resourcesInfo.getResourceUrl()
						.trim());
				resourceInfo.setAppId(resourcesInfo.getAppId().trim());
				resourceList.add(resourceInfo);
			}
			resourceManageService.resourceRemove(operator.trim(), resourceList);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "群成员添加成功！");
			map.put("data", "{}");
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/remove交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/remove交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/remove交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/remove交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	@RequestMapping(value = "/resource/team/apply", method = RequestMethod.POST)
	public Map<String, Object> resourceTeamApply(HttpServletRequest request) {
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim())) {
			throw new BusinessException("请求条件不允许为空！");
		}

		try {
			ResourceTeamApplyRequestDTO requestDTO = (ResourceTeamApplyRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data,
							ResourceTeamApplyRequestDTO.class);
			String applyUser = requestDTO.getApplyUser();
			String teamId = requestDTO.getTeamId();
			String appId = requestDTO.getAppId();
			String applyDescription = requestDTO.getApplyDescription();
			if (applyUser == null || "".equals(applyUser.trim())) {
				throw new BusinessException("申请人不允许为空！");
			}
			if (teamId == null || "".equals(teamId.trim())) {
				throw new BusinessException("群组编码不允许为空！");
			}
			if (appId == null || "".equals(appId.trim())) {
				throw new BusinessException("应用编码不允许为空！");
			}
			if (applyDescription == null || "".equals(applyDescription.trim())) {
				throw new BusinessException("申请信息描述不允许为空！");
			}

			ResourceApproval resourceApproval = new ResourceApproval();
			resourceApproval.setTeamId(Integer.parseInt(teamId.trim()));
			resourceApproval.setAppId(appId.trim());
			resourceApproval.setApplyUser(applyUser.trim());
			resourceApproval.setApplyDescription(applyDescription);
			resourceManageService.resourceTeamApply(resourceApproval);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "群成员移除成功！");
			map.put("data", "{}");
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/apply交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/apply交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/apply交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/apply交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	@RequestMapping(value = "/resource/team/approval", method = RequestMethod.POST)
	public Map<String, Object> resourceTeamApproval(HttpServletRequest request) {
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim())) {
			throw new BusinessException("请求条件不允许为空！");
		}

		try {
			ResourceTeamApprovalRequestDTO requestDTO = (ResourceTeamApprovalRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data,
							ResourceTeamApprovalRequestDTO.class);
			String applyId = requestDTO.getApplyId();
			String approvalUser = requestDTO.getApprovalUser();
			String approvalResult = requestDTO.getApprovalResult();
			String approvalDescription = requestDTO.getApprovalDescription();
			List<ResourcesInfoRequestDTO> resourcesInfoList = requestDTO
					.getResourcesInfo();
			if (applyId == null || "".equals(applyId.trim())) {
				throw new BusinessException("权限申请流水号不允许为空！");
			}
			if (approvalUser == null || "".equals(approvalUser.trim())) {
				throw new BusinessException("授权审核人不允许为空！");
			}
			if (approvalResult == null || "".equals(approvalResult.trim())) {
				throw new BusinessException("授权结果不允许为空！");
			}

			ResourceApproval resourceApproval = new ResourceApproval();
			resourceApproval.setApplyId(Integer.parseInt(applyId.trim()));
			resourceApproval.setApprovalUser(approvalUser.trim());
			resourceApproval.setApprovalResult(approvalResult.trim());
			resourceApproval.setApprovalDescription(approvalDescription);

			List<ResourceInfo> resourceList = new ArrayList<ResourceInfo>();
			if (MDConstants.APPROVAL_SUCCESS.equals(approvalResult.trim())) {
				if (resourcesInfoList == null || resourcesInfoList.size() < 1) {
					throw new BusinessException("资源信息不允许为空！");
				}
				for (ResourcesInfoRequestDTO resourcesInfo : resourcesInfoList) {
					if (resourcesInfo.getResourceUrl() == null
							|| "".equals(resourcesInfo.getResourceUrl().trim())) {
						throw new BusinessException("资源URL不允许为空！");
					}
					ResourceInfo resourceInfo = new ResourceInfo();
					resourceInfo.setResourceUrl(resourcesInfo.getResourceUrl()
							.trim());
					resourceList.add(resourceInfo);
				}
			}
			resourceManageService.resourceTeamApproval(resourceApproval,
					resourceList);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "群组信息更改成功！");
			map.put("data", "{}");
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/approval交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/approval交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/approval交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/team/approval交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	@RequestMapping(value = "/resource/member/verification", method = RequestMethod.GET)
	public Map<String, Object> resourceMemberVerification(
			HttpServletRequest request) {
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim())) {
			throw new BusinessException("请求条件不允许为空！");
		}

		try {
			ResourceMemberVerificationRequestDTO requestDTO = (ResourceMemberVerificationRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data,
							ResourceMemberVerificationRequestDTO.class);
			String memberId = requestDTO.getMemberId();
			String resourceUrl = requestDTO.getResourceUrl();
			String appId = requestDTO.getAppId();
			if (memberId == null || "".equals(memberId.trim())) {
				throw new BusinessException("用户编码不允许为空！");
			}
			if (resourceUrl == null || "".equals(resourceUrl.trim())) {
				throw new BusinessException("资源URL不允许为空！");
			}
			if (appId == null || "".equals(appId.trim())) {
				throw new BusinessException("资源所属系统不允许为空！");
			}
			ResourceMemberInfo resourceMemberInfo = new ResourceMemberInfo();
			resourceMemberInfo.setMemberId(memberId.trim());
			resourceMemberInfo.setResourceUrl(resourceUrl.trim());
			resourceMemberInfo.setAppId(appId.trim());
			boolean result = resourceQueryService
					.resourceMemberVerification(resourceMemberInfo);

			ResourceMemberVerificationResponseDTO responseDTO = new ResourceMemberVerificationResponseDTO();
			responseDTO.setVerifyResult(result ? APIConstants.VERIFY_SUCCESS
					: APIConstants.VERIFY_FAIL);
			responseDTO.setVerifyDescription(result ? "用户拥有访问该资源的权限"
					: "用户没有访问该资源的权限");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "管理员权限移交成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/member/verification交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/member/verification交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/member/verification交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("/api/v1/resource/member/verification交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		}

	}
}

/*
 * 修改历史 $Log$
 */