<%@ page language="java" import="java.util.*" isELIgnored="false"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../common/common.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MD</title>
<style>
.el-menu-vertical-demo:not (.el-menu--collapse ) {
	width: 200px;
}

.el-header {
	width: 100%;
	background-color: #B3C0D1;
    color: #333;
	line-height: 60px;
	text-align: center;
	position: fixed; 
 	top:0px;
}

.el-footer {
	width: 100%;
	background-color: #B3C0D1;
    color: #333;
	line-height: 60px;
	text-align: center;
 	position: fixed; 
 	bottom:0px; 
}

.el-aside {
/*     background-color: #D3DCE6; */
    color: #333;
    position: fixed; 
 	top:60px;
 	bottom:60px;
}

.el-main {
width: 100%;
/*     background-color: #E9EEF3; */
    color: #333;
/*     text-align: center; */
/*     line-height: 160px; */
    position: fixed; 
    left:310px;
 	top:60px;
 	bottom:60px;
  }
</style>

</head>
<body>
	<div id="app">
		<el-container>
		    <el-header>Header</el-header>
		    <el-container>
		        <el-aside>
		            <el-radio-group v-model="isCollapse" style="margin-bottom: 20px;">
                        <el-radio-button :label="false">展开</el-radio-button>
                        <el-radio-button :label="true">收起</el-radio-button>
                    </el-radio-group>
                    <el-menu class="el-menu-vertical-demo" :collapse="isCollapse">
                        <el-submenu index="1">
                            <template slot="title">
                                <i class="el-icon-menu"></i>
                                <span slot="title">菜单1</span>
                            </template>
                            <el-menu-item index="1-1">菜单1-1</el-menu-item>
                            <el-menu-item index="1-2">菜单1-2</el-menu-item>
                            <el-menu-item index="1-3">菜单1-3</el-menu-item>
                            <el-submenu index="1-4">
                                <template slot="title">
                                    <i class="el-icon-menu"></i>
                                    <span slot="title">菜单1-4</span>
                                </template>
                                <el-menu-item index="1-4-1">菜单1-4-1</el-menu-item>
                                <el-menu-item index="1-4-2">菜单1-4-2</el-menu-item>
                            </el-submenu>
                        </el-submenu>
                        <el-submenu index="2">
                            <template slot="title">
                                <i class="el-icon-menu"></i>
                                <span slot="title">菜单2</span>
                            </template>
                            <el-menu-item index="2-1">菜单2-1</el-menu-item>
                            <el-menu-item index="2-2">菜单2-2</el-menu-item>
                            <el-menu-item index="2-3">菜单2-3</el-menu-item>
                        </el-submenu>
                        <el-submenu index="3">
                            <template slot="title">
                                <i class="el-icon-menu"></i>
                                <span slot="title">菜单3</span>
                            </template>
                            <el-submenu index="3-1">
                                <template slot="title">
                                    <i class="el-icon-menu"></i>
                                    <span slot="title">菜单3-1</span>
                                </template>
                                <el-menu-item index="3-1-1">菜单3-1-1</el-menu-item>
                                <el-menu-item index="3-1-2">菜单3-1-2</el-menu-item>
                            </el-submenu>
                        </el-submenu>
                        <el-menu-item index="4">
                            <i class="el-icon-menu"></i>
                            <span slot="title">菜单4</span>
                        </el-menu-item>
                    </el-menu>
			    </el-aside>
		        <el-main>
                    <el-tabs type="card" closable >
                        <el-tab-pane label="首页">首页</el-tab-pane>
                        <el-tab-pane label="消息中心">消息中心</el-tab-pane>
                        <el-tab-pane label="角色管理">角色管理</el-tab-pane>
                        <el-tab-pane label="定时任务补偿">定时任务补偿</el-tab-pane>
                    </el-tabs>
                </el-main>
		    </el-container>
		    <el-footer>Footer</el-footer>
		<el-container>
	</div>
</body>

<script>
	new Vue({
		el : "#app",
		data : {
			isCollapse : false,
			editableTabsValue2 : '2',
			editableTabs2 : [ {
				title : 'Tab 1',
				name : '1',
				content : 'Tab 1 content'
			}, {
				title : 'Tab 2',
				name : '2',
				content : 'Tab 2 content'
			} ],
			tabIndex : 2
		},
		methods : {
			addTab : function(targetName) {
// 				newTabName = ++this.tabIndex + '';
// 				this.editableTabs2.push({
// 					title : 'New Tab',
// 					name : newTabName,
// 					content : 'New Tab content'
// 				});
// 				this.editableTabsValue2 = newTabName;
			},
			removeTab : function(targetName) {
// 				tabs = this.editableTabs2;
// 				activeName = this.editableTabsValue2;
// 				if (activeName === targetName) {
// 					// 	            tabs.forEach((tab, index)) {
// 					// 	              if (tab.name === targetName) {
// 					// 	                let nextTab = tabs[index + 1] || tabs[index - 1];
// 					// 	                if (nextTab) {
// 					// 	                  activeName = nextTab.name;
// 					// 	                }
// 					// 	              }
// 					// 	            });
// 				}

				// 	          this.editableTabsValue2 = activeName;
				// 	          this.editableTabs2 = tabs.filter(tab = tab.name !== targetName);
			}
		}
	});
</script>
</html>