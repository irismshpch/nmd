<%@ page language="java" import="java.util.*" isELIgnored="false" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<link rel="shortcut icon" type="image/x-icon" href="${ctx}/static/image/logo.ico" media="screen" />
<link rel="stylesheet" type="text/css" href="${ctx}/static/scripts/element-ui/lib/theme-chalk/index.css">  
<script type="text/javascript" src="${ctx}/static/scripts/vue.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/vue-router.js"></script>  
<script type="text/javascript" src="${ctx}/static/scripts/axios.js"></script>  
<script type="text/javascript" src="${ctx}/static/scripts/element-ui/lib/index.js"></script>  

<script type="text/javascript">
    var ctx = "${ctx}";
</script>