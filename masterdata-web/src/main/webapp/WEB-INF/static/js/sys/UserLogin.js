new Vue({
	el : '#app',
	data : {
		mbr_id : 'sunxiaodong001',
		pwd : 'abcd1234*'
	},
	created: function() {
		console.log("created");
		axios.get(ctx + '/view/v1/mbr/logout').then(function(r){  
			if(r.result_code != "0000") {
				this.$alert(r.result_message, "提示", {
					confirmButtonText: "确定",
					type : 'warning'
				});
			}
		}).catch(function(r){  
			alert(r); // catch中element-ui的函数无法使用，不知道是否是因为加载顺序的问题
		});
	},
	methods : {
		submit : function(event) {
			// 获取值
			var mbr_id = this.mbr_id;
			var pwd = this.pwd;
			if (mbr_id == '' || pwd == '') {
				this.$alert("用户名或密码不能为空", "提示", {
//					confirmButtonText: "确定",
					type : 'warning'
				});
				return;
			}
			
			axios.get(ctx + '/view/v1/mbr/login', {  
			    params : { //请求参数  
			        data : {
			        	mbr_id: mbr_id,
			        	pwd:pwd
			        } 
			    }  
			}).then(function(r){  
				if(r.result_code == "0000") {
					window.location.href= ctx + "/view/v1/user/home";
				} else {
					this.$alert(r.data。result_code, "提示", {
//						confirmButtonText: "确定",
						type : 'warning'
					});
				}
			       
			}).catch(function(r){  
				alert(r); // axios中element-ui的函数无法使用，不知道是否是因为加载顺序的问题
			});  
			
		}
	}
})