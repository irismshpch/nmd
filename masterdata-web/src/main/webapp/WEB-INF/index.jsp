<%@ page language="java" import="java.util.*" isELIgnored="false"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ include file="./views/common/common.jsp"%>
<html>  
<head>  
<title>MD</title>  
<style>  
.el-row {  
    margin-bottom: 20px;  
    &:last-child {  
      margin-bottom: 0;  
    }  
  }  
    .login-box {  
        margin-top:20%;  
        margin-left:40%;  
    }  
</style>  
</head>  
<body>  
<div class="login-box" id="app" >  
 <el-row>  
    <el-col :span="8">  
        <el-input id="mbr_id"  v-model="mbr_id" placeholder="请输入帐号">  
            <template slot="prepend">帐号</template>  
        </el-input>   
    </el-col>  
 </el-row>  
 <el-row>  
    <el-col :span="8">  
        <el-input id="pwd" v-model="pwd" type="password" placeholder="请输入密码">  
            <template slot="prepend">密码</template>  
        </el-input>  
    </el-col>  
 </el-row>  
 <el-row>  
    <el-col :span="8">  
        <el-button id="login" v-on:click="submit" style="width:100%" type="primary">登录</el-button>  
    </el-col>  
 </el-row>  

</div> 
</body>  
  
<script type="text/javascript" src="${ctx}/static/js/sys/UserLogin.js?v=201806041743"></script> 
</html>  