/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年5月31日
 *******************************************************************************/

package com.hualife.md.web.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hualife.md.constants.APIConstants;
import com.hualife.md.constants.MDConstants;
import com.hualife.md.member.dao.mybatis.entity.MemberInfo;
import com.hualife.md.member.dto.lite.MbrLoginRequestDTO;
import com.hualife.md.member.service.IMemberManageService;
import com.hualife.support.exception.BusinessException;
import com.hualife.support.exception.SysException;
import com.hualife.support.util.JsonUtil;

/**
 * @description: 为了便于后续前后端分离，接口请求都全部转发至其他模块下对应的restapi接口
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@RestController
@RequestMapping(value = "/view/v1")
public class InternalController {
	private static final Logger logger = LoggerFactory
			.getLogger(InternalController.class);


	@Autowired
	private IMemberManageService memberManageService;

	@RequestMapping(value = "/mbr/login", method = RequestMethod.GET)
	public Map<String, Object> memberLogin(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");
		
		if (session != null) {
			if (session.getAttribute(MDConstants.MD_USER) != null) {
				throw new BusinessException("当前已存在登录用户！");
			}
		}
//		String code = (String) session.getAttribute("code");
//		if(code == null || "".equals(code)) {
//			throw new BusinessException("验证码已失效，请刷新验证码！");
//		}
//		if(!code.equalsIgnoreCase(srand)){
//			throw new BusinessException("验证码错误！");
//		}

		try {
			MbrLoginRequestDTO requestDTO = (MbrLoginRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, MbrLoginRequestDTO.class);
			MemberInfo memberInfo = memberManageService.login(requestDTO.getMbrId(),
					requestDTO.getPwd());
			session.setAttribute(MDConstants.MD_USER, memberInfo);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(APIConstants.RESULT_CODE, APIConstants.RESULT_SUCCESS);
			map.put(APIConstants.RESULT_MESSAGE, "登录成功！");
			map.put(APIConstants.DATA, null);
			return map;
		} catch (BusinessException e) {
			throw new BusinessException(e.getMessage());
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/view/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/view/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/view/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}

	}
	
	@RequestMapping(value = "/mbr/logout", method = RequestMethod.GET)
	public Map<String, Object> memberLogout(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		
		if (session != null) {
			if (session.getAttribute(MDConstants.MD_USER) != null) {
				session.removeAttribute(MDConstants.MD_USER);
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(APIConstants.RESULT_CODE, APIConstants.RESULT_SUCCESS);
		map.put(APIConstants.RESULT_MESSAGE, "退出成功！");
		map.put(APIConstants.DATA, null);
		return map;
	}
}

/*
 * 修改历史 $Log$
 */