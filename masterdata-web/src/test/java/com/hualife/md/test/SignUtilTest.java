/**
 * 
 */
package com.hualife.md.test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.UUID;

import com.hualife.support.util.MD5Util;

/**
 * @author sunxiaodong001
 * 
 */
public class SignUtilTest {
	private static final String ENCODING = "UTF-8";
	
	private static final String SEPARATOR_AND = "&";
	
	private static final String SEPARATOR_EQUAL = "=";

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";

	private static String formatDate(Date date) {
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		df.setTimeZone(new SimpleTimeZone(0, "GMT"));
		System.out.println(df.format(date));
		return df.format(date);
	}

	private static String percentEncode(String value)
			throws UnsupportedEncodingException {
		return value != null ? URLEncoder.encode(value, ENCODING)
				.replace("+", "%20").replace("*", "%2A").replace("%7E", "~")
				: null;
	}

	public static String sign(Map<String, String> parameters,String privateKey)
			throws UnsupportedEncodingException {
		// 对参数进行排序
		String[] sortedKeys = parameters.keySet().toArray(new String[] {});
		Arrays.sort(sortedKeys);

		// 生成stringToSign字符串
		StringBuilder stringToSign = new StringBuilder();
		stringToSign.append(privateKey).append(SEPARATOR_AND);

		StringBuilder canonicalizedQueryString = new StringBuilder();
		for (String key : sortedKeys) {
			// 这里注意对key和value进行编码
			canonicalizedQueryString.append(SEPARATOR_AND).append(percentEncode(key))
					.append(SEPARATOR_EQUAL).append(percentEncode(parameters.get(key)));
		}

		// 这里注意对canonicalizedQueryString进行编码
		stringToSign.append(percentEncode(canonicalizedQueryString.toString()));
		String sign = MD5Util.sign(stringToSign.toString(), ENCODING);
		System.out.println(stringToSign + "-------------" + sign);
		return sign;
	}

	/**
	 * @param args
	 * @throws UnsupportedEncodingException
	 */
	public static void main(String[] args) throws UnsupportedEncodingException {
		Map<String, String> parameters = new HashMap<String, String>();
		// 加入请求参数
		parameters.put("app_id", "www.md.com");
//		parameters.put("timestamp", formatDate(new Date()));
		parameters.put("timestamp","2018-02-06 08:13:25:667");
		parameters.put("sign_type", "md5");
//		parameters.put("nonce", UUID.randomUUID().toString());
		parameters.put("nonce", "4ebf7ea9-6723-4382-9294-061ab6a6a479");
		
		parameters.put("data", "{\"mbr_id\":\"110002145\"}");
//		parameters.put("data", "{\"code\":\"21064.21521\",\"chn_id\":\"0\",\"type\":\"2\"}");

		// 对参数进行排序
		String[] sortedKeys = parameters.keySet().toArray(new String[] {});
		Arrays.sort(sortedKeys);

		final String SEPARATOR = "&";
		String privateKey = "hFlzTjvIeGb8_a9ysL1RkNDKS5YogPO3dQExnqB76mCru0UAcX";

		// 生成stringToSign字符串
		StringBuilder stringToSign = new StringBuilder();
		stringToSign.append(privateKey);

		StringBuilder canonicalizedQueryString = new StringBuilder();
		for (String key : sortedKeys) {
			// 这里注意对key和value进行编码
			canonicalizedQueryString.append(SEPARATOR).append(percentEncode(key))
					.append("=").append(percentEncode(parameters.get(key)));
		}
		System.out.println(stringToSign + "-------------" + canonicalizedQueryString.toString());
		// 这里注意对canonicalizedQueryString进行编码
		stringToSign.append(percentEncode(canonicalizedQueryString.toString()));
		String sign = MD5Util.sign(stringToSign.toString(), ENCODING);
		System.out.println(canonicalizedQueryString.append(SEPARATOR).append("sign")
				.append("=").append(sign).toString());
//		System.out.println(stringToSign + "-------------" + sign);

	}

}
