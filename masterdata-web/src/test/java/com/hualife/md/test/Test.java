/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年1月25日
 *******************************************************************************/

package com.hualife.md.test;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 每一个Ticket表示一个用户的请求
		Ticket ticket01 = TicketFactory.getTicket("北京", "厦门");
		ticket01.showTicketInfo("卧铺");
		Ticket ticket02 = TicketFactory.getTicket("北京", "厦门");
		ticket02.showTicketInfo("硬座");
		Ticket ticket03 = TicketFactory.getTicket("北京", "厦门");
		ticket03.showTicketInfo("软卧");
	}

}

/*
 * 修改历史 $Log$
 */