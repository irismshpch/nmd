/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年1月25日
 *******************************************************************************/


package com.hualife.md.test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description: 共享模式：共享工厂类
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class TicketFactory {
	static Map<String,Ticket> stringTicketMap=new ConcurrentHashMap<>();
    public static Ticket getTicket(String from,String to){
        String key=from+"-"+to;//使用出发地和目的地作为Map的key
        if(stringTicketMap.containsKey(key)){
        	System.out.println("TAG：使用缓存==> "+key);
            return stringTicketMap.get(key);//获取以出发地和目的地为key的对象
        }else {
        	System.out.println("TAG：创建对象==> "+key);
            Ticket ticket=new TrainTicket(from, to);//创建对象
            stringTicketMap.put(key,ticket);
            return ticket;
        }
    }
}

/*
 * 修改历史
 * $Log$ 
 */