/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年2月6日
 *******************************************************************************/

package com.hualife.md.test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hualife.md.team.service.impl.WechatTagInterface;
import com.hualife.support.exception.BusinessException;
import com.hualife.support.exception.SysException;
import com.hualife.support.util.EncoderUtil;
import com.hualife.support.util.HttpUtil;
import com.hualife.support.util.JsonUtil;
import com.hualife.support.util.RandomUtils;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class WechatUserSynTest extends BaseJunit4Test {

	String timestamp = String.valueOf(System.currentTimeMillis()); // 时间戳
	String nonce = RandomUtils.generateRandomCharAndNumber(32); // 32位随机字符串

	private final static String I_RESULT = "result_code";
	private final static String I_MESSAGE = "message";
	private final static String I_FAIL = "fail";

	private static final String SOURCE = "ZSJPT";
	private static final Logger logger = LoggerFactory
			.getLogger(WechatUserSynTest.class);

	@Autowired
	private WechatTagInterface wechatTagInterface;

	@SuppressWarnings("unchecked")
	@Test
	public void test() {
	/*	Map<String, String> tokenMap = getToken();
		if (null == tokenMap || tokenMap.size() == 0) {
			logger.info("获取token失败");// 错误信息
			return;
		}
		String result = tokenMap.get("result_code");
		if (result.equals(CodeTable.I_FAIL)) {
			logger.info("获取token失败【{}】", JsonUtil.objectToJackson(tokenMap));// 错误信息
			return;

		}
		String token = tokenMap.get("access_token");
		logger.debug("access_token:" + token);
		try {
			// String tagId = "15";
			String[] arr = { "12", "13", "14", "15" };// 标签id--测试使用
			for (int i = 0; i < arr.length; i++) {
				Map<String, Object> wechartMap = wechatTagInterface
						.queryTagUser(arr[i], token);

				if (wechartMap == null || wechartMap.size() == 0)
					return;
				else {
					logger.debug(wechartMap.toString());
					if (wechartMap.get("userlist") == null) {
						// 全部添加
					} else {
						StringBuffer bf = new StringBuffer();
						List<Map<String, String>> wechartUserList = (List<Map<String, String>>) wechartMap
								.get("userlist");

						for (Map<String, String> wechartUser : wechartUserList) {
							String userid = wechartUser.get("userid");
							String name = wechartUser.get("name");
							System.out.println(userid);
							bf.append(userid + " " + name + "\n");
						}
						
						

						List<Integer> partList = new ArrayList<Integer>();
						partList = jackson2Object(wechartMap.get("partylist")
								+ "", partList.getClass());

						// 循环获取部门内的成员
						for (Integer str : partList) {
							Map<String, Object> partMap = queryPartUser(str + "", token);
							List<Map<String, String>> partUserList = (List<Map<String, String>>) partMap.get("userlist");
							for (Map<String, String> user : partUserList) {
								String userid = user.get("userid");
								String name = user.get("name");
								bf.append(userid + " " + name + "\n");
							}
						}
						// 保存数据到文件--测试
						try {
							FileWriter fileWriter = new FileWriter("D:\\" + arr[i] + ".txt");
							fileWriter.write(bf.toString());
							fileWriter.flush();
							fileWriter.close();
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}*/

	}

	@SuppressWarnings("unchecked")
	public static <T> T jackson2Object(String json, Class<?> object) {
		T t = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			t = (T) mapper.readValue(json, object);
		} catch (JsonParseException e) {
			logger.error("把json字符串转换为JavaBean对象异常", e);
			logger.info("传入JSON：" + json);
			throw new SysException("jackson转化对象失败");
		} catch (JsonMappingException e) {
			logger.error("把json字符串转换为JavaBean对象异常", e);
			logger.info("传入JSON：" + json);
			throw new SysException("jackson转化对象失败");
		} catch (IOException e) {
			logger.error("把json字符串转换为JavaBean对象异常", e);
			logger.info("传入JSON：" + json);
			throw new SysException("jackson转化对象失败");
		}
		return t;
	}

	public Map<String, String> getToken() {
		Map<String, String> result = new HashMap<String, String>();

		Map<String, String> paraMap = getWeChatHead();
		Map<String, String> map = doRequest(paraMap);
		if (map.get(I_RESULT) != null) {
			return map;
		} else {
			result.put(I_RESULT, I_FAIL);
			result.put(I_MESSAGE, "接口返回内容有误");
			logger.info("接口返回内容有误：" + map.toString());
		}
		return result;
	}

	protected Map<String, String> getWeChatHead() {

		String key = "PFB369ZY7WBN74W95FN088QV4UK9FUNU";//生产
		String signature = EncoderUtil.Md5_32(key + timestamp + nonce + SOURCE)
				.toUpperCase();
		Map<String, String> param = new HashMap<String, String>();
		param.put("timestamp", timestamp);
		param.put("nonce", nonce);
		param.put("trade_source", SOURCE);
		param.put("signature", signature);
		return param;
	}

	@SuppressWarnings("unchecked")
	protected Map<String, String> doRequest(Map<String, String> paraMap) {
		Map<String, String> result = new HashMap<String, String>();
		/* 查询加密验签KEY */
		String urlValue = "http://qyhgateway.ihxlife.com/api/v1/token/get";
		// 查询接口数据
		String json = HttpUtil.post(urlValue, paraMap, 10000);
		if (json == null || "".equals(json.trim())) {
			result.put("result_code", "fail");
			result.put("result_msg", "接口返回值为空");
			return result;
		} else {
			result = JsonUtil.jackson2Map(json);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> queryTagUser(String tagId, String token) {

		Map<String, Object> map = new HashMap<String, Object>();
		String url = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token="
				+ token + "&tagid=" + tagId;
		try {
			String resultJson = HttpUtil.get(url);
			if (resultJson == null || "".equals(resultJson.trim())) {
				logger.info("创建标签失败，未返回数据");
				throw new BusinessException("创建标签失败，企业号未返回数据！");
			}
			map = JsonUtil.jackson2Map(resultJson);
			return map;
		} catch (Exception e) {
			logger.info("查询标签下用户失败，请求异常【{}】", e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> queryPartUser(String tagId, String token) {
		// access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD&status=STATUS
		Map<String, Object> map = new HashMap<String, Object>();
		String url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist"
				+ "?access_token=" + token
				+ "&status=0&fetch_child=1&department_id=" + tagId;
		try {
			String resultJson = HttpUtil.get(url);
			if (resultJson == null || "".equals(resultJson.trim())) {
				logger.info("创建标签失败，未返回数据");
				throw new BusinessException("创建标签失败，企业号未返回数据！");
			}
			map = JsonUtil.jackson2Map(resultJson);
			return map;
		} catch (Exception e) {
			logger.info("查询标签下用户失败，请求异常【{}】", e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}

/*
 * 修改历史 $Log$
 */