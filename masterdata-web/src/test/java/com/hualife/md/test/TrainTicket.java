/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年1月25日
 *******************************************************************************/

package com.hualife.md.test;

import java.util.Random;

/**
 * @description: 共享模式：具体共享实现类
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class TrainTicket implements Ticket {
	public String from; // 始发地
	public String to;// 目的地
	public String siteType;// 座位type
	public int price;// 价格

	public TrainTicket(String from, String to) {// 构造方法
		this.from = from;
		this.to = to;
	}

	public void showTicketInfo(String siteType) {
		price = getPrice(siteType);// 获取价格
		System.out.println("TAG：购买 从" + from + "到" + to + "的" + siteType
				+ "的火车票" + ",价格：" + price);
	}

	private int getPrice(String siteType) {
		return new Random().nextInt(365);
	}
}

/*
 * 修改历史 $Log$
 */