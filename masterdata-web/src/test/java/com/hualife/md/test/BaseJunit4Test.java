/**
 * 
 */
package com.hualife.md.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author sunxiaodong001
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
		"classpath*:com/hualife/md/test/config/spring.xml",
		"classpath*:com/hualife/md/test/config/spring-mvc.xml" })
public class BaseJunit4Test {
	@Before
    public void init() {
        //在运行测试之前的业务代码
    }
	
	@Test
	public void test(){
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    @After
    public void after() {
        //在测试完成之后的业务代码
    }
}