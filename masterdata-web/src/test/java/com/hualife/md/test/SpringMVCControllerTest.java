/**
 * 
 */
package com.hualife.md.test;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.hualife.support.util.MD5Util;
import com.hualife.support.util.SignatureUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author sunxiaodong001
 * 
 */
@WebAppConfiguration
public class SpringMVCControllerTest extends BaseJunit4Test {
	private static final String ENCODING = "UTF-8";
	private static final String SEPARATOR_AND = "&";

	private static final String SEPARATOR_EQUAL = "=";

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";

	protected MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext wac;

	private static String formatDate(Date date) {
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		df.setTimeZone(new SimpleTimeZone(0, "GMT"));
		System.out.println(df.format(date));
		return df.format(date);
	}

	@Before()
	// 这个方法在每个方法执行之前都会执行一遍
	public void setup() {
		// webAppContextSetup 注意上面的static import
		// webAppContextSetup 构造的WEB容器可以添加fileter 但是不能添加listenCLASS
		// WebApplicationContext context =
		// ContextLoader.getCurrentWebApplicationContext();
		// 如果控制器包含如上方法 则会报空指针
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		this.mockMvc = (webAppContextSetup(this.wac).addFilter(
				characterEncodingFilter, "/*")).build(); // 初始化MockMvc对象

	}

	/**
	 * @throws Exception
	 */
	@Test
	public void test() {
		try {

//			String appId = "www.md.com";
			String appId = "HXSYS0001";
			String signType = "md5";
//			String timestamp = formatDate(new Date());
//			String nonce = UUID.randomUUID().toString();
			String timestamp = "1505885956264";
			String nonce = "3715UbPl9U139As33i3d";
//			String data = "{\"team_id\":\"100\",\"organization_code\":\"86\",\"current_level\":\"Y\"}";
//			String data = "{\"mbr_id\":\"114000002\"}"; //328000944
//			String data = "{\"organization_code\":\"000000036944\"}"; //328000944  000000036944
//			String data = "{\"search_terms\":\"孙小\",\"search_operator\":\"sunxiaokai\"}";
			String data = "{\"code\":\"86\",\"chn_id\":\"1\",\"type\":\"1\",\"grd_id\":[\"BC\"]}"; 
//			String data = "{\"team_id\":\"0\",\"operator\":\"sunxiaodong001\",\"members_info\":[{\"member_id\":\"zhangyuliang\"},{\"member_id\":\"huangfang\"},{\"member_id\":\"wangyong\"}]}"; 
//			String data = "{\"search_terms\":\"宋露露\",\"chn_id\":\"0\",\"search_operator\":\"liwei007\"}"; 
//			String data = "{\"search_terms\":\"孙小\",\"search_operator\":\"zhouqiuhong\"}"; 
//			String data = "{\"mbr_id\":\"sunxiaodong001\",\"pwd\":\"abcd12341*\"}"; 
			Map<String, String> parameters = new HashMap<String, String>();
			String page = "{\"page_index\":2,\"page_size\":5}";
//			String page = "";
			// 加入请求参数
			parameters.put("app_id", appId);
			parameters.put("timestamp", timestamp);
			parameters.put("sign_type", signType);
			parameters.put("nonce", nonce);
			if(!"".equals(page))
			  parameters.put("page", page);
			parameters.put("data", data);

//			String privateKey = "hFlzTjvIeGb8_a9ysL1RkNDKS5YogPO3dQExnqB76mCru0UAcX";
			String privateKey = "a12dadf_12asdfaguoif213zdqfqe3ADq1da249jbb_10AF1As";

			String sign = SignatureUtil.sign(parameters, privateKey);
			String responseStr;

//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/team/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/mbr/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/mbr/chn/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/orgs/mbrs/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
			responseStr = mockMvc
					.perform(
							(get("/api/v1/orgs/mbrs/query").param("app_id", appId)
									.param("sign_type", signType)
									.param("sign", sign)
									.param("timestamp", timestamp)
									.param("nonce", nonce).param("data", data).param("page", page)
									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
					.andExpect(status().isOk()).andDo(print()).andReturn()
					.getResponse().getContentAsString();
//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/orgs/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
			
//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/mbrs/fulltext/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
//			responseStr = mockMvc
//					.perform(
//							(get("/api/v1/orgs/mbrs/query").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
//			responseStr = mockMvc
//			.perform(
//					(get("/api/v1/mbr/team/query").param("app_id", appId)
//							.param("sign_type", signType)
//							.param("sign", sign)
//							.param("timestamp", timestamp)
//							.param("nonce", nonce).param("data", data)
//							.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//			.andExpect(status().isOk()).andDo(print()).andReturn()
//			.getResponse().getContentAsString();
			
//			responseStr = mockMvc
//					.perform(
//							(post("/api/v1/team/foundation").param("app_id", appId)
//									.param("sign_type", signType)
//									.param("sign", sign)
//									.param("timestamp", timestamp)
//									.param("nonce", nonce).param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();
			
//			responseStr = mockMvc
//					.perform(
//							(get("/view/v1/mbr/home").param("data", data)
//									.contentType(MediaType.APPLICATION_FORM_URLENCODED)))
//					.andExpect(status().isOk()).andDo(print()).andReturn()
//					.getResponse().getContentAsString();

			System.out.println(responseStr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();                                   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
