/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年1月25日
 *******************************************************************************/


package com.hualife.md.test;

/**
 * @description: 共享模式：抽象共享类
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface Ticket {
	//根据座位的类型展示车票结果信息
	public void showTicketInfo(String siteType);
}

/*
 * 修改历史
 * $Log$ 
 */