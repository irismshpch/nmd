/**
 * 
 */
package com.hualife.md.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.hualife.md.common.service.IWeChatGatewayService;
import com.hualife.md.common.service.IWeChatTagService;
import com.hualife.md.common.service.IWeChatUserService;
import com.hualife.md.member.service.IActiveDirectoryUserSyn2MDService;
import com.hualife.md.member.service.IActiveDirectoryUserTransferService;
import com.hualife.md.member.service.IMemberManageService;
import com.hualife.md.member.service.IOrganizationIntegrateService;
import com.hualife.md.team.service.IWechatUserSynService;


/**
 * @author sunxiaodong001
 *
 */
public class SpringMVCServiceTest extends BaseJunit4Test {
	@Autowired
	private IActiveDirectoryUserSyn2MDService activeDirectoryUserSyn2MDService ;
	
	@Autowired
	private IActiveDirectoryUserTransferService activeDirectoryUserTransferService ;
	
	@Autowired
	private IOrganizationIntegrateService organizationIntegrateService ;
	@Autowired
	private IWechatUserSynService wechatUserSynService ;
	@Autowired
	private IWeChatGatewayService WeChatGatewayService ;
	@Autowired
	private IWeChatTagService weChatTagService ;
	
	@Autowired
	private IMemberManageService memberManageService ;
	
	@Test
	public void test(){
		try {    
			String startTime = get_HHmmssms();
//			activeDirectoryUserSyn2MDService.syn2Md();
//			activeDirectoryUserSyn2MDService.synOU2Md();;
//			activeDirectoryUserTransferService.transfer();
//			activeDirectoryUserTransferService.transferOU();
//			organizationIntegrateService.integrate();
//			organizationIntegrateService.transData();
//			organizationIntegrateService.integrateOU();
//			wechatUserSynService.syn2Wechat();
//			String token = WeChatGatewayService.getToken();
//			List<String> userList = weChatTagService.queryTagUser("86120118", token);
//			System.out.println("企业号返回结果："+ userList.toString());	
//			String endTime = get_HHmmssms();
//			int longTime = Integer.valueOf(endTime)-Integer.valueOf(startTime);
//			System.out.println("用时为："+ longTime);	
//			memberManageService.login("sunxiaodong001", "abcd1234*");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String get_HHmmssms() {
		String dt = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HHmmssms");
			Calendar cal = Calendar.getInstance();
			dt = sdf.format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dt;
	}
}
