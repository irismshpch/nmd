/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年11月1日
 *******************************************************************************/

package com.hualife.md.test;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class DNTest {
	public static void main(String args[]) {
//		String dnstr = "CN=卜树婷,OU=江苏电商座席,OU=江苏多元业务部,OU=江苏分公司本部,OU=江苏分公司";
		String dnstr = "CN=卜树婷,OU=江苏电商座席,OU=江苏多元业务部";

		dnstr = dnstr.substring(dnstr.indexOf(",") + 1);

		System.out.println(dnstr);
		StringBuffer ou = new StringBuffer();
		String[] dnlist = dnstr.split(",");
		int i = 0;
		for (String dn : dnlist) {
			if (dn.startsWith("OU=") && i < 2) {
				String value = dn.substring("OU=".length());
				ou.insert(0, value);
				ou.insert(0, "/");
				i++;
			}
		}
		if (ou.length() > 0)
			ou.deleteCharAt(0);
		System.out.println(ou.toString());

		StringBuffer ou2 = new StringBuffer();
		String[] dnlist2 = dnstr.split(",");
		for (String dn : dnlist2) {
			if (dn.startsWith("OU=")) {
				String value = dn.substring("OU=".length());
				ou2.insert(0, value);
				ou2.insert(0, "/");
			}
		}
		if (ou2.length() > 0)
			ou2.deleteCharAt(0);
		System.out.println(ou2.toString());
		
		StringBuffer ou3 = new StringBuffer();
		String[] dnlist3 = dnstr.split(",");
		for (String dn : dnlist3) {
			if (dn.startsWith("OU=")) {
				String value = dn.substring("OU=".length());
				ou3.insert(0, value);
				ou3.insert(0, "/");
			}
		}
		if (ou3.length() > 0){
			ou3.deleteCharAt(0);
			if(ou3.lastIndexOf("/")>0){
			ou3.delete(ou3.lastIndexOf("/"), ou3.length());
			System.out.println(ou3.toString());}
			else {
				System.out.println("null");
			}
		}
			
		
	}
}

/*
 * 修改历史 $Log$
 */