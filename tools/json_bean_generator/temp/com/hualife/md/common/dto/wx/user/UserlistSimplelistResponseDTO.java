/**
 * This class is generated by the generator.
 * Any question, please contact to sunxiaodong(13401146791).
 */

package com.hualife.md.common.dto.wx.user;

import java.util.List;

/**
 * auto generate UserlistSimplelistResponseDTO model
 * 
 * @author generator
 * @date 2018-05-15
 */
public class UserlistSimplelistResponseDTO {
	private String userid;
	private String name;
	private List<Integer> department;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getDepartment() {
		return department;
	}

	public void setDepartment(List<Integer> department) {
		this.department = department;
	}

}