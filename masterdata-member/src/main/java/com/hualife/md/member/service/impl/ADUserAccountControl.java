/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/

package com.hualife.md.member.service.impl;

/**
 * @description: userAccountControl：该属性标志是累积性的（吐血） 如514=512+2=账号存在且关闭；
 *               66048=65536+512=密码永不过期+账号正常
 *               说明：http://blog.sina.com.cn/s/blog_a1c8858c0101f73i.html
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class ADUserAccountControl {

	// private static int SCRIPT = 1; // 运行登录脚本
	private static int ACCOUNTDISABLE = 2; // 账户禁用
	private static int HOMEDIR_REQUIRED = 8; // 账户需要HOME目录
	private static int LOCKOUT = 16; // 账户被锁定
	private static int PASSWD_NOTREQD = 32; // 无需密码
	private static int PASSWD_CANT_CHANGE = 64; // 用户不可更改密码（只读）
	private static int ENCRYPTED_TEXT_PWD_ALLOWED = 128; // 允许发送加密密码
	private static int TEMP_DUPLICATE_ACCOUNT = 256; // 启用临时本地账户
	private static int NORMAL_ACCOUNT = 512; // 典型用户
	private static int INTERDOMAIN_TRUST_ACCOUNT = 2048; // 信任域间可信账户
	private static int WORKSTATION_TRUST_ACCOUNT = 4096; // 工作站计算机
	private static int SERVER_TRUST_ACCOUNT = 8192; // 域控制器
	private static int DONT_EXPIRE_PASSWORD = 65536; // 密码永不过期
	private static int MNS_LOGON_ACCOUNT = 131072; // 多数节点集（MNS）登录
	private static int SMARTCARD_REQUIRED = 262144; // 需要智能卡登录
	private static int TRUSTED_FOR_DELEGATION = 524288; // 允许进行Kerberos委派
	private static int NOT_DELEGATED = 1048576; // 禁止安全正文的委派
	private static int USE_DES_KEY_ONLY = 2097152; // 强制使用DES加密的密钥
	private static int DONT_REQ_PREAUTH = 4194304; // 登录时无需Kerberos验证
	private static int PASSWORD_EXPIRED = 8388608; // 密码已过期
	private static int TRUSTED_TO_AUTH_FOR_DELEGATION = 16777216; // 允许该帐户进行委派

	public static boolean checkAccountDisable(int userAccContr) {
		if (userAccContr >= TRUSTED_TO_AUTH_FOR_DELEGATION) {
			userAccContr = userAccContr - TRUSTED_TO_AUTH_FOR_DELEGATION;
		}
		if (userAccContr >= PASSWORD_EXPIRED) {
			userAccContr = userAccContr - PASSWORD_EXPIRED;
		}
		if (userAccContr >= DONT_REQ_PREAUTH) {
			userAccContr = userAccContr - DONT_REQ_PREAUTH;
		}
		if (userAccContr >= USE_DES_KEY_ONLY) {
			userAccContr = userAccContr - USE_DES_KEY_ONLY;
		}
		if (userAccContr >= NOT_DELEGATED) {
			userAccContr = userAccContr - NOT_DELEGATED;
		}
		if (userAccContr >= TRUSTED_FOR_DELEGATION) {
			userAccContr = userAccContr - TRUSTED_FOR_DELEGATION;
		}
		if (userAccContr >= SMARTCARD_REQUIRED) {
			userAccContr = userAccContr - SMARTCARD_REQUIRED;
		}
		if (userAccContr >= MNS_LOGON_ACCOUNT) {
			userAccContr = userAccContr - MNS_LOGON_ACCOUNT;
		}
		if (userAccContr >= DONT_EXPIRE_PASSWORD) {
			userAccContr = userAccContr - DONT_EXPIRE_PASSWORD;
		}
		if (userAccContr >= SERVER_TRUST_ACCOUNT) {
			userAccContr = userAccContr - SERVER_TRUST_ACCOUNT;
		}
		if (userAccContr >= WORKSTATION_TRUST_ACCOUNT) {
			userAccContr = userAccContr - WORKSTATION_TRUST_ACCOUNT;
		}
		if (userAccContr >= INTERDOMAIN_TRUST_ACCOUNT) {
			userAccContr = userAccContr - INTERDOMAIN_TRUST_ACCOUNT;
		}
		if (userAccContr >= NORMAL_ACCOUNT) {
			userAccContr = userAccContr - NORMAL_ACCOUNT;
		}
		if (userAccContr >= TEMP_DUPLICATE_ACCOUNT) {
			userAccContr = userAccContr - TEMP_DUPLICATE_ACCOUNT;
		}
		if (userAccContr >= ENCRYPTED_TEXT_PWD_ALLOWED) {
			userAccContr = userAccContr - ENCRYPTED_TEXT_PWD_ALLOWED;
		}
		if (userAccContr >= PASSWD_CANT_CHANGE) {
			userAccContr = userAccContr - PASSWD_CANT_CHANGE;
		}
		if (userAccContr >= PASSWD_NOTREQD) {
			userAccContr = userAccContr - PASSWD_NOTREQD;
		}
		if (userAccContr >= LOCKOUT) {
			userAccContr = userAccContr - LOCKOUT;
		}
		if (userAccContr >= HOMEDIR_REQUIRED) {
			userAccContr = userAccContr - HOMEDIR_REQUIRED;
		}
		if (userAccContr >= ACCOUNTDISABLE) {
			return true;
		}
		return false;

	}

}

/*
 * 修改历史 $Log$
 */