package com.hualife.md.member.dto;

public class OrganizationalunitInfoDTO {
	private String ouId;
	private String ouName;
	private String affiliationOuId;
	private String affiliationOuName;
	private String wholeOuId;
	private String wholeOuName;
	private String weight;
	private String totalNum;
	private String createTime;
	private String updateTime;
	public OrganizationalunitInfoDTO() {
		super();
	}
	public OrganizationalunitInfoDTO(String ouId, String ouName, String affiliationOuId, String affiliationOuName,
			String wholeOuId, String wholeOuName, String weight, String totalNum, String createTime,
			String updateTime) {
		super();
		this.ouId = ouId;
		this.ouName = ouName;
		this.affiliationOuId = affiliationOuId;
		this.affiliationOuName = affiliationOuName;
		this.wholeOuId = wholeOuId;
		this.wholeOuName = wholeOuName;
		this.weight = weight;
		this.totalNum = totalNum;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}
	public String getOuId() {
		return ouId;
	}
	public void setOuId(String ouId) {
		this.ouId = ouId;
	}
	public String getOuName() {
		return ouName;
	}
	public void setOuName(String ouName) {
		this.ouName = ouName;
	}
	public String getAffiliationOuId() {
		return affiliationOuId;
	}
	public void setAffiliationOuId(String affiliationOuId) {
		this.affiliationOuId = affiliationOuId;
	}
	public String getAffiliationOuName() {
		return affiliationOuName;
	}
	public void setAffiliationOuName(String affiliationOuName) {
		this.affiliationOuName = affiliationOuName;
	}
	public String getWholeOuId() {
		return wholeOuId;
	}
	public void setWholeOuId(String wholeOuId) {
		this.wholeOuId = wholeOuId;
	}
	public String getWholeOuName() {
		return wholeOuName;
	}
	public void setWholeOuName(String wholeOuName) {
		this.wholeOuName = wholeOuName;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(String totalNum) {
		this.totalNum = totalNum;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	
}
