/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/

package com.hualife.md.member.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hualife.md.constants.APIConstants;
import com.hualife.md.constants.MDConstants;
import com.hualife.md.member.dao.ex.entity.MemberIntegrateInfo;
import com.hualife.md.member.dao.mybatis.entity.MemberInfo;
import com.hualife.md.member.dao.mybatis.entity.OrganizationInfo;
import com.hualife.md.member.dao.mybatis.entity.OrganizationalunitInfo;
import com.hualife.md.member.dao.mybatis.entity.SynLisLaagent;
import com.hualife.md.member.dto.lite.MbrLoginRequestDTO;
import com.hualife.md.member.dto.lite.MbrRequestDTO;
import com.hualife.md.member.dto.lite.MbrResponseDTO;
import com.hualife.md.member.dto.lite.MbrsFulltextRequestDTO;
import com.hualife.md.member.dto.lite.MbrsFulltextResponseDTO;
import com.hualife.md.member.dto.lite.OrgsMbrsRequestDTO;
import com.hualife.md.member.dto.lite.OrgsMbrsResponseDTO;
import com.hualife.md.member.dto.lite.OrgsRequestDTO;
import com.hualife.md.member.dto.lite.OrgsResponseDTO;
import com.hualife.md.member.dto.mbrchn.ChnPwrResponseDTO;
import com.hualife.md.member.dto.mbrchn.MbrChnRequestDTO;
import com.hualife.md.member.dto.mbrchn.MbrChnResponseDTO;
import com.hualife.md.member.service.IMemberManageService;
import com.hualife.md.member.service.IMemberQueryService;
import com.hualife.support.exception.BusinessException;
import com.hualife.support.exception.SysException;
import com.hualife.support.util.JsonUtil;
import com.hualife.support.util.PageDTO;
import com.hualife.support.util.PageResultDTO;

/**
 * @description: 删减版的接口，主要目的是为了减少手机端的流量和内存压力
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@RestController
@RequestMapping(value = "/api/v1")
public class MemberLiteController {
	private static final Logger logger = LoggerFactory
			.getLogger(MemberLiteController.class);

	@Autowired
	private IMemberQueryService memberQueryService;

	@Autowired
	private IMemberManageService memberManageService;

	@RequestMapping(value = "/mbr/query", method = RequestMethod.GET)
	public Map<String, Object> memberQuery(HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");

		try {
			MbrRequestDTO requestDTO = (MbrRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, MbrRequestDTO.class);
			MemberInfo member = new MemberInfo();
			member.setMemberId(requestDTO.getMbrId());
			MemberIntegrateInfo memberInfo = memberQueryService
					.queryGatherMemberInfo(member);
			if (memberInfo == null) {
				throw new BusinessException("查不到该用户信息！");
			}
			MbrResponseDTO responseDTO = new MbrResponseDTO();
			responseDTO.setMbrId(memberInfo.getMemberId());
			responseDTO.setMbrName(memberInfo.getMemberName());
			responseDTO.setUnit(getOrganizationUnit(memberInfo
					.getOrganizationUnit()));
			responseDTO.setOrgCode(memberInfo.getAffiliationCode());
			responseDTO.setOrgName(memberInfo.getAffiliationName());
			responseDTO.setOuCode(memberInfo.getAffiorgunitCode());// TODO
			responseDTO.setOuName(memberInfo.getAffiorgunitName());// TODO
			responseDTO.setChnId(memberInfo.getChannel());
			responseDTO.setGrdId(memberInfo.getGrade());
			responseDTO.setGrdName(memberInfo.getGradeName());

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	@RequestMapping(value = "/mbr/chn/query", method = RequestMethod.GET)
	public Map<String, Object> memberChannelQuery(HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");

		try {
			MbrChnRequestDTO requestDTO = (MbrChnRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, MbrChnRequestDTO.class);
			MemberInfo member = new MemberInfo();
			member.setMemberId(requestDTO.getMbrId());
			MemberIntegrateInfo memberInfo = memberQueryService
					.queryGatherMemberInfo(member);
			if (memberInfo == null) {
				throw new BusinessException("查不到该用户信息！");
			}
			MbrChnResponseDTO responseDTO = new MbrChnResponseDTO();
			responseDTO.setMbrId(memberInfo.getMemberId());

			List<ChnPwrResponseDTO> chnPwrList = new ArrayList<ChnPwrResponseDTO>();
			ChnPwrResponseDTO selfChnPwr = new ChnPwrResponseDTO();
			selfChnPwr.setChnId(memberInfo.getChannel());
			selfChnPwr.setChnName(memberInfo.getChannelName());
			chnPwrList.add(selfChnPwr);
			if ("0".equals(memberInfo.getChannel())
					&& memberInfo.getAffiliationCode() != null
					&& !"".equals(memberInfo.getAffiliationCode().trim())) {
				List<MemberIntegrateInfo> resultList = memberQueryService
						.queryChannelProxy(requestDTO.getMbrId());
				for (MemberIntegrateInfo result : resultList) {
					ChnPwrResponseDTO chnPwr = new ChnPwrResponseDTO();
					chnPwr.setChnId(result.getChannel());
					chnPwr.setChnName(result.getChannelName());
					chnPwrList.add(chnPwr);
				}
			}
			responseDTO.setChnPwr(chnPwrList);// TODO

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/chn/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/chn/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/chn/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	@RequestMapping(value = "/orgs/query", method = RequestMethod.GET)
	public Map<String, Object> organizationsQuery(HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");

		try {
			OrgsRequestDTO requestDTO = (OrgsRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, OrgsRequestDTO.class);
			if (requestDTO.getCode() == null
					|| "".equals(requestDTO.getCode().trim())
					|| requestDTO.getChnId() == null
					|| "".equals(requestDTO.getChnId().trim()))
				throw new BusinessException("查询条件不允许为空！");
			String wholeCode = requestDTO.getCode().trim();
			String channelId = requestDTO.getChnId().trim();
			String type = requestDTO.getType();
			if (type != null) {
				type = type.trim();
				if (!MDConstants.HIERARCHY_CMS.equals(type)
						&& !MDConstants.HIERARCHY_DOMAIN.equals(type)) {
					throw new BusinessException("层级类型不合法！");
				}
				if (!"0".equals(channelId)
						&& MDConstants.HIERARCHY_DOMAIN.equals(type)) {
					throw new BusinessException("层级类型不合法！");
				}
			} else {
				type = MDConstants.HIERARCHY_CMS;
			}

			OrgsResponseDTO responseDTO = new OrgsResponseDTO();
			responseDTO.setCode(wholeCode);
			if (MDConstants.HIERARCHY_CMS.equals(type)) {
				List<OrganizationInfo> organizationResult = memberQueryService
						.queryOrganizationInfo(wholeCode);
				if (organizationResult != null
						&& organizationResult.size() == 1) {
					responseDTO.setName(organizationResult.get(0)
							.getOrganizationShortname());
				} else {
					throw new BusinessException("查不到有效的机构信息！");
				}

				int sunNum = 0;
				List<OrgsResponseDTO.MbrResponseDTO> membersInfoDTOList = new ArrayList<OrgsResponseDTO.MbrResponseDTO>();
				List<OrgsResponseDTO.SubOrgResponseDTO> subOrganizationList = new ArrayList<OrgsResponseDTO.SubOrgResponseDTO>();
				if (channelId.equals(organizationResult.get(0).getChannel())) { // 如果要查询的渠道编码跟机构所属渠道编码一致，则可以继续查询当前层级用户和下级机构信息
					if ("0".equals(channelId)) { // 内勤
						MemberInfo member = new MemberInfo();
						member.setOrganizationCode(organizationResult.get(0)
								.getOrganizationCode());
						List<MemberIntegrateInfo> memberInfoResultList = memberQueryService
								.queryMemberInfos(wholeCode, channelId, "Y",
										null, null);
						sunNum += memberInfoResultList.size();
						for (MemberIntegrateInfo memberInfoResult : memberInfoResultList) {
							OrgsResponseDTO.MbrResponseDTO membersInfoDTO = responseDTO.new MbrResponseDTO();
							membersInfoDTO.setMbrId(memberInfoResult
									.getMemberId());
							membersInfoDTO.setMbrName(memberInfoResult
									.getMemberName());
							membersInfoDTO.setUnit(memberInfoResult
									.getOrganizationUnit());
							membersInfoDTO.setOrgCode(memberInfoResult
									.getAffiliationCode());
							membersInfoDTO.setOuCode(memberInfoResult
									.getAffiorgunitCode());
							membersInfoDTOList.add(membersInfoDTO);
						}

					} else {
						SynLisLaagent laagent = new SynLisLaagent();
						laagent.setAgentgroup(organizationResult.get(0)
								.getOrganizationCode());
						List<SynLisLaagent> laagentResultList = memberQueryService
								.queryLaagentInfos(laagent);
						sunNum += laagentResultList.size();
						for (SynLisLaagent laagentResult : laagentResultList) {
							OrgsResponseDTO.MbrResponseDTO membersInfoDTO = responseDTO.new MbrResponseDTO();
							membersInfoDTO.setMbrId(laagentResult
									.getAgentcode());
							membersInfoDTO.setMbrName(laagentResult.getName());
							membersInfoDTO
									.setUnit(getOrganizationUnit(organizationResult
											.get(0).getWholeOrganizationName()));
							membersInfoDTO.setOrgCode(organizationResult.get(0)
									.getWholeOrganizationCode());
							membersInfoDTOList.add(membersInfoDTO);
						}

						responseDTO.setMbr(membersInfoDTOList);
					}

					List<OrganizationInfo> organizationListResult = memberQueryService
							.querySubOrganizationInfos(wholeCode, channelId);
					for (OrganizationInfo subOrganizationResult : organizationListResult) {
						OrgsResponseDTO.SubOrgResponseDTO subOrganization = responseDTO.new SubOrgResponseDTO();
						subOrganization.setCode(subOrganizationResult
								.getWholeOrganizationCode());
						subOrganization.setName(subOrganizationResult
								.getOrganizationShortname());

						int totalNumber = subOrganizationResult
								.getTotalNumber();
						if (totalNumber == 0)
							continue;
						sunNum += totalNumber;
						subOrganization.setNum(totalNumber);
						subOrganizationList.add(subOrganization);
					}

				} else if ("0".equals(organizationResult.get(0).getChannel())) { // 如果要查询的渠道编码跟机构所属渠道编码不一致，且当前渠道是内勤渠道，可以继续向下查询子机构信息，但无查询当前层级用户的必要
					List<OrganizationInfo> organizationListResult = memberQueryService
							.querySubOrganizationInfos(wholeCode);
					for (OrganizationInfo subOrganizationResult : organizationListResult) {
						OrgsResponseDTO.SubOrgResponseDTO subOrganization = responseDTO.new SubOrgResponseDTO();
						subOrganization.setCode(subOrganizationResult
								.getWholeOrganizationCode());
						subOrganization.setName(subOrganizationResult
								.getOrganizationShortname());
						int totalNumber = 0;
						if ("0".equals(subOrganizationResult.getChannel())) {
							totalNumber = memberQueryService
									.queryTotalLaagentsByInside(
											subOrganizationResult
													.getWholeOrganizationCode(),
											channelId);
						} else if (channelId.equals(subOrganizationResult
								.getChannel())) {
							totalNumber = subOrganizationResult
									.getTotalNumber();
						}

						if (totalNumber == 0)
							continue;

						sunNum += totalNumber;
						subOrganization.setNum(totalNumber);
						subOrganizationList.add(subOrganization);
					}
				}
				responseDTO.setMbr(membersInfoDTOList);
				responseDTO.setNum(sunNum);

				responseDTO.setSubOrg(subOrganizationList);
			} else {
				List<OrganizationalunitInfo> organizationResult = memberQueryService
						.queryOrganizationalunitInfo(wholeCode);
				if (organizationResult != null
						&& organizationResult.size() == 1) {
					responseDTO.setName(organizationResult.get(0).getOuName());
				} else {
					throw new BusinessException("查不到有效的机构信息！");
				}

				int sunNum = 0;
				List<OrgsResponseDTO.MbrResponseDTO> membersInfoDTOList = new ArrayList<OrgsResponseDTO.MbrResponseDTO>();
				List<OrgsResponseDTO.SubOrgResponseDTO> subOrganizationList = new ArrayList<OrgsResponseDTO.SubOrgResponseDTO>();
				MemberInfo member = new MemberInfo();
				member.setOrganizationunitCode(organizationResult.get(0)
						.getOuId());
				List<MemberIntegrateInfo> memberInfoResultList = memberQueryService
						.queryGatherMemberInfos(wholeCode, "Y", null);
				sunNum += memberInfoResultList.size();
				for (MemberIntegrateInfo memberInfoResult : memberInfoResultList) {
					OrgsResponseDTO.MbrResponseDTO membersInfoDTO = responseDTO.new MbrResponseDTO();
					membersInfoDTO.setMbrId(memberInfoResult.getMemberId());
					membersInfoDTO.setMbrName(memberInfoResult.getMemberName());
					membersInfoDTO.setUnit(memberInfoResult
							.getOrganizationUnit());
					membersInfoDTO.setOrgCode(memberInfoResult
							.getAffiliationCode());
					membersInfoDTO.setOuCode(memberInfoResult
							.getAffiorgunitCode());
					membersInfoDTOList.add(membersInfoDTO);
				}

				List<OrganizationalunitInfo> organizationListResult = memberQueryService
						.querySubOrganizationalunitInfos(wholeCode);
				for (OrganizationalunitInfo subOrganizationResult : organizationListResult) {
					OrgsResponseDTO.SubOrgResponseDTO subOrganization = responseDTO.new SubOrgResponseDTO();
					subOrganization.setCode(subOrganizationResult
							.getWholeOuId());
					subOrganization.setName(subOrganizationResult.getOuName());

					int totalNumber = subOrganizationResult.getTotalNum();
					if (totalNumber == 0)
						continue;
					sunNum += totalNumber;
					subOrganization.setNum(totalNumber);
					subOrganizationList.add(subOrganization);
				}

				responseDTO.setMbr(membersInfoDTOList);
				responseDTO.setNum(sunNum);

				responseDTO.setSubOrg(subOrganizationList);

			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/orgs/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/orgs/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/orgs/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}
	}

	@RequestMapping(value = "/orgs/mbrs/query", method = RequestMethod.GET)
	public Map<String, Object> organizationsMembersQuery(
			HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		String page = request.getParameter("page");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");

		boolean pagination = false; // 是否需要分页
		int total = 0;
		try {
			OrgsMbrsRequestDTO requestDTO = (OrgsMbrsRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, OrgsMbrsRequestDTO.class);
			if (requestDTO.getCode() == null
					|| "".equals(requestDTO.getCode().trim())
					|| requestDTO.getChnId() == null
					|| "".equals(requestDTO.getChnId().trim()))
				throw new BusinessException("查询条件不允许为空！");

			PageDTO pageDTO = new PageDTO();
			PageResultDTO pageResultDTO = new PageResultDTO();
			if (page != null && !"".equals(page.trim())) {
				pageDTO = (PageDTO) JsonUtil.SnakeJson2CamelBean(page,
						PageDTO.class);
				if (pageDTO.getPageIndex() > 0) {
					pagination = true;
				}
			}

			String wholeCode = requestDTO.getCode().trim();
			String channelId = requestDTO.getChnId().trim();
			String type = requestDTO.getType();
			List<String> gradeList = requestDTO.getGrdId();
			if (type != null) {
				type = type.trim();
				if (!MDConstants.HIERARCHY_CMS.equals(type)
						&& !MDConstants.HIERARCHY_DOMAIN.equals(type)) {
					throw new BusinessException("层级类型不合法！");
				}
				if (!"0".equals(channelId)
						&& MDConstants.HIERARCHY_DOMAIN.equals(type)) {
					throw new BusinessException("层级类型不合法！");
				}
			} else {
				type = MDConstants.HIERARCHY_CMS;
			}

			OrgsMbrsResponseDTO responseDTO = new OrgsMbrsResponseDTO();
			responseDTO.setCode(wholeCode);

			if (MDConstants.HIERARCHY_CMS.equals(type)) {
				List<OrganizationInfo> organizationResult = memberQueryService
						.queryOrganizationInfo(wholeCode);
				if (organizationResult != null
						&& organizationResult.size() == 1) {
				} else {
					throw new BusinessException("查不到有效的机构信息！");
				}
				List<OrgsMbrsResponseDTO.MbrResponseDTO> membersInfoDTOList = new ArrayList<OrgsMbrsResponseDTO.MbrResponseDTO>();
				if (channelId.equals(organizationResult.get(0).getChannel())
						|| "0".equals(organizationResult.get(0).getChannel())) { // 如果要查询的渠道编码跟机构所属渠道编码一致，或当前渠道是内勤渠道，则可以继续查询当前层级和下级机构用户信息
					MemberInfo member = new MemberInfo();
					member.setOrganizationCode(organizationResult.get(0)
							.getOrganizationCode());
					List<MemberIntegrateInfo> memberInfoResultList = new ArrayList<MemberIntegrateInfo>();
					if (pagination) {
						memberInfoResultList = memberQueryService
								.queryMemberInfos(wholeCode, channelId, "N",
										gradeList, pageDTO);
						total = memberQueryService.queryTotalMemberInfos(
								wholeCode, channelId, "N", gradeList, pageDTO);
						System.out.println("total:"+total);
						pageResultDTO.setTotal(total);
						pageResultDTO.setPageNum((total - 1)
								/ pageDTO.getPageSize() + 1);
					} else {
						memberInfoResultList = memberQueryService
								.queryMemberInfos(wholeCode, channelId, "N",
										gradeList, null);
					}

					for (MemberIntegrateInfo memberInfoResult : memberInfoResultList) {
						OrgsMbrsResponseDTO.MbrResponseDTO membersInfoDTO = responseDTO.new MbrResponseDTO();
						membersInfoDTO.setMbrId(memberInfoResult.getMemberId());
						membersInfoDTO.setMbrName(memberInfoResult
								.getMemberName());
						if ("0".equals(channelId))
							membersInfoDTO.setUnit(memberInfoResult
									.getOrganizationUnit());
						else
							membersInfoDTO.setUnit(this
									.getOrganizationUnit(memberInfoResult
											.getOrganizationUnit()));
						membersInfoDTO.setOrgCode(memberInfoResult
								.getAffiliationCode());
						membersInfoDTO.setOuCode(memberInfoResult
								.getAffiorgunitCode());
						membersInfoDTO.setChnId(memberInfoResult.getChannel());
						membersInfoDTOList.add(membersInfoDTO);
					}

				}

				responseDTO.setMbr(membersInfoDTOList);
			} else {
				List<OrganizationalunitInfo> organizationResult = memberQueryService
						.queryOrganizationalunitInfo(wholeCode);
				if (organizationResult != null
						&& organizationResult.size() == 1) {
				} else {
					throw new BusinessException("查不到有效的机构信息！");
				}

				List<OrgsMbrsResponseDTO.MbrResponseDTO> membersInfoDTOList = new ArrayList<OrgsMbrsResponseDTO.MbrResponseDTO>();
				MemberInfo member = new MemberInfo();
				member.setOrganizationunitCode(organizationResult.get(0)
						.getOuId());

				List<MemberIntegrateInfo> memberInfoResultList = new ArrayList<MemberIntegrateInfo>();
				if (pagination) {
					memberInfoResultList = memberQueryService
							.queryGatherMemberInfos(wholeCode, "N", pageDTO);
					total = memberQueryService.queryTotalGatherMemberInfos(
							wholeCode, "N", pageDTO);
					System.out.println("total:"+total);
					pageResultDTO.setTotal(total);
					pageResultDTO.setPageNum((total - 1)
							/ pageDTO.getPageSize() + 1);
				} else {
					memberInfoResultList = memberQueryService
							.queryGatherMemberInfos(wholeCode, "N", null);
				}
				for (MemberIntegrateInfo memberInfoResult : memberInfoResultList) {
					OrgsMbrsResponseDTO.MbrResponseDTO membersInfoDTO = responseDTO.new MbrResponseDTO();
					membersInfoDTO.setMbrId(memberInfoResult.getMemberId());
					membersInfoDTO.setMbrName(memberInfoResult.getMemberName());
					membersInfoDTO.setUnit(memberInfoResult
							.getOrganizationUnit());
					membersInfoDTO.setOrgCode(memberInfoResult
							.getAffiliationCode());
					membersInfoDTO.setOuCode(memberInfoResult
							.getAffiorgunitCode());
					membersInfoDTO.setChnId("0");
					membersInfoDTOList.add(membersInfoDTO);
				}

				responseDTO.setMbr(membersInfoDTOList);

			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			if (pagination) {
				map.put("page", JsonUtil.CamelBean2SnakeJson(pageResultDTO));
			}

			return map;

		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/orgs/mbrs/query交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/orgs/mbrs/query交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/orgs/mbrs/query交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		}
	}

	@RequestMapping(value = "/mbrs/fulltext/query", method = RequestMethod.GET)
	public Map<String, Object> membersFulltextQuery(HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");

		try {
			MbrsFulltextRequestDTO requestDTO = (MbrsFulltextRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, MbrsFulltextRequestDTO.class);
			List<MemberIntegrateInfo> memberInfoResultList = memberQueryService
					.queryMemberIntegrateInfos(requestDTO.getSearchTerms(),
							requestDTO.getChnId(),
							requestDTO.getSearchOperator());
			if (memberInfoResultList == null
					|| memberInfoResultList.size() == 0) {
				throw new BusinessException("查不到用户信息！");
			}

			MbrsFulltextResponseDTO responseDTO = new MbrsFulltextResponseDTO();
			List<MbrsFulltextResponseDTO.MbrResponseDTO> mbrResponseDTOList = new ArrayList<MbrsFulltextResponseDTO.MbrResponseDTO>();
			for (MemberIntegrateInfo memberInfoResult : memberInfoResultList) {
				MbrsFulltextResponseDTO.MbrResponseDTO mbrDTO = responseDTO.new MbrResponseDTO();
				mbrDTO.setMbrId(memberInfoResult.getMemberId());
				mbrDTO.setMbrName(memberInfoResult.getMemberName());
				mbrDTO.setUnit(memberInfoResult.getOrganizationUnit());
				mbrDTO.setOrgCode(memberInfoResult.getAffiliationCode());
				mbrDTO.setOrgName(memberInfoResult.getAffiliationName());
				mbrDTO.setOuCode(memberInfoResult.getAffiorgunitCode());
				mbrDTO.setOuName(memberInfoResult.getAffiorgunitName());
				mbrDTO.setChnId(memberInfoResult.getChannel());
				mbrResponseDTOList.add(mbrDTO);
			}
			responseDTO.setMbr(mbrResponseDTOList);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbrs/fulltext/query交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbrs/fulltext/query交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbrs/fulltext/query交易处理失败，原因是【{}】.",
					e.getMessage());
			throw new SysException(e.getMessage());
		}

	}

	private String getOrganizationUnit(String wholeUnitStr) {
		String[] splitstr = wholeUnitStr.split(MDConstants.SEPARATOR_SLANT);
		int size = splitstr.length;
		if (size <= 2)
			return wholeUnitStr;
		else {
			StringBuffer unit = new StringBuffer();
			unit.append(splitstr[size - 2]).append(MDConstants.SEPARATOR_SLANT)
					.append(splitstr[size - 1]);
			return unit.toString();
		}
	}

	@RequestMapping(value = "/mbr/login", method = RequestMethod.GET)
	public Map<String, Object> memberLogin(HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim()))
			throw new BusinessException("查询条件不允许为空！");

		try {
			MbrLoginRequestDTO requestDTO = (MbrLoginRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, MbrLoginRequestDTO.class);
			memberManageService.login(requestDTO.getMbrId(), requestDTO.getPwd());
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(APIConstants.RESULT_CODE, APIConstants.RESULT_SUCCESS);
			map.put(APIConstants.RESULT_MESSAGE, "查询成功！");
			map.put(APIConstants.DATA, "{}");
			return map;
		} catch (BusinessException e) {
			throw new BusinessException(e.getMessage());
		}  catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/mbr/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}

	}
}

/*
 * 修改历史 $Log$
 */