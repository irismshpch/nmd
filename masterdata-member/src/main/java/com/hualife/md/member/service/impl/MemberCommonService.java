/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年12月1日
 *******************************************************************************/

package com.hualife.md.member.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hualife.md.member.dao.mybatis.entity.MemberInfo;
import com.hualife.md.member.dao.mybatis.entity.MemberInfoExample;
import com.hualife.md.member.dao.mybatis.entity.SynLisLaagent;
import com.hualife.md.member.dao.mybatis.entity.SynLisLaagentExample;
import com.hualife.md.member.dao.mybatis.entity.SynLisLabranchgroup;
import com.hualife.md.member.dao.mybatis.entity.SynLisLabranchgroupExample;
import com.hualife.md.member.dao.mybatis.entity.SynLisLdcom;
import com.hualife.md.member.dao.mybatis.entity.SynLisLdcomExample;
import com.hualife.md.member.dao.mybatis.mapper.MemberInfoMapper;
import com.hualife.md.member.dao.mybatis.mapper.SynLisLaagentMapper;
import com.hualife.md.member.dao.mybatis.mapper.SynLisLabranchgroupMapper;
import com.hualife.md.member.dao.mybatis.mapper.SynLisLdcomMapper;

/**
 * @description: 用户模块公共功能和抽取（friendly类型）
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("memberCommonService")
class MemberCommonService {

	@Autowired
	private SynLisLdcomMapper synLisLdcomMapper;
	@Autowired
	private SynLisLabranchgroupMapper synLislabranchgroupMapper;
	@Autowired
	private MemberInfoMapper memberInfoMapper;
	@Autowired
	private SynLisLaagentMapper synLisLaagentMapper;

	public SynLisLdcom queryLdcom(String comcode) {
		if (comcode == null || "".equals(comcode.trim()))
			return null;
		return synLisLdcomMapper.selectByPrimaryKey(comcode.trim());
	}

	/**
	 * 查询下级管理机构
	 * 
	 * @param comcode
	 * @return
	 */
	List<SynLisLdcom> querySubLdcoms(String comcode) {
		if (comcode == null || "".equals(comcode.trim()))
			return null;
		SynLisLdcomExample example = new SynLisLdcomExample();
		SynLisLdcomExample.Criteria criteria = example.createCriteria();
		criteria.andComcodeLike(comcode.trim() + "__");
		return synLisLdcomMapper.selectByExample(example);
	}
	
	List<SynLisLdcom> queryAllBranchs() {
		SynLisLdcomExample example = new SynLisLdcomExample();
		SynLisLdcomExample.Criteria criteria = example.createCriteria();
		criteria.andComcodeLike("________");
		return synLisLdcomMapper.selectByExample(example);
	}
	
	/**
	 * 查询下级展业机构
	 * 
	 * @param comcode
	 * @return
	 */
	List<SynLisLabranchgroup> querySubLabranchgroups(String comcode) {
		if (comcode == null || "".equals(comcode.trim()))
			return null;
		return querySubLabranchgroups(comcode.trim(), null);
	}

	List<SynLisLabranchgroup> querySubLabranchgroups(String comcode,
			String agentgroup) {
		if ((comcode == null || "".equals(comcode.trim()))
				&& (agentgroup == null || "".equals(agentgroup.trim())))
			return null;
		SynLisLabranchgroupExample example = new SynLisLabranchgroupExample();
		SynLisLabranchgroupExample.Criteria criteria = example.createCriteria();
		if (comcode != null && !"".equals(comcode.trim())) {
			criteria.andManagecomEqualTo(comcode.trim());
		}
		if (agentgroup != null && !"".equals(agentgroup.trim())) {
			criteria.andUpbranchEqualTo(agentgroup.trim());
		}
		return synLislabranchgroupMapper.selectByExample(example);
	}

	List<MemberInfo> queryMemberInfos(String comcode) {
		if (comcode == null || "".equals(comcode.trim()))
			return null;
		MemberInfoExample example = new MemberInfoExample();
		MemberInfoExample.Criteria criteria = example.createCriteria();
		criteria.andOrganizationCodeEqualTo(comcode.trim());
		return memberInfoMapper.selectByExample(example);
	}

	List<SynLisLaagent> queryLaagentInfos(String agentgroup) {
		if (agentgroup == null || "".equals(agentgroup.trim()))
			return null;
		SynLisLaagentExample example = new SynLisLaagentExample();
		SynLisLaagentExample.Criteria criteria = example.createCriteria();
		criteria.andAgentgroupEqualTo(agentgroup.trim());
		List<String> state = new ArrayList<String>();
		state.add("01");
		state.add("02");
		criteria.andAgentstateIn(state);
		return synLisLaagentMapper.selectByExample(example);
	}
}

/*
 * 修改历史 $Log$
 */