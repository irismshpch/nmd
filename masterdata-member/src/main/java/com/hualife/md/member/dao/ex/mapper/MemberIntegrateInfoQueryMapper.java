/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年11月22日
 *******************************************************************************/


package com.hualife.md.member.dao.ex.mapper;

import java.util.List;

import com.hualife.md.member.dao.ex.entity.MemberIntegrateInfo;
import com.hualife.support.util.PageDTO;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface MemberIntegrateInfoQueryMapper {
	MemberIntegrateInfo selectMemberInfoById(String memberId);
	MemberIntegrateInfo selectGatherMemberInfoById(String memberId);
	MemberIntegrateInfo selectLaagentInfoById(String agentcode);
	int selectTotalMembersByAffiliation(String affiliationCode);
	List<MemberIntegrateInfo> selectMembersByName(MemberIntegrateInfo memberIntegrateInfo);
	List<MemberIntegrateInfo> selectGatherMembersByName(MemberIntegrateInfo memberIntegrateInfo);
	List<MemberIntegrateInfo> selectLaagentsByName(MemberIntegrateInfo memberIntegrateInfo);
	int selectTotalLaagentsByInside(MemberIntegrateInfo memberIntegrateInfo);
	List<MemberIntegrateInfo> selectMemberInfosByTerms(PageDTO pageDTO);
	List<MemberIntegrateInfo> selectMemberInfosByTermsPage(PageDTO pageDTO);
	int selectTotalMemberInfosByTermsPage(PageDTO pageDTO);
	List<MemberIntegrateInfo> selectMemberInfosByLevel(MemberIntegrateInfo memberIntegrateInfo);
	List<MemberIntegrateInfo> selectGatherMemberInfosByTerms(PageDTO pageDTO);
	List<MemberIntegrateInfo> selectGatherMemberInfosByTermsPage(PageDTO pageDTO);
	int selectTotalGatherMemberInfosByTermsPage(PageDTO pageDTO);
	List<MemberIntegrateInfo> selectGatherMemberInfosByLevel(MemberIntegrateInfo memberIntegrateInfo);
	List<MemberIntegrateInfo> selectLaagentInfosByTerms(PageDTO pageDTO);
	List<MemberIntegrateInfo> selectLaagentInfosByTermsPage(PageDTO pageDTO);
	int selectTotalLaagentInfosByTermsPage(PageDTO pageDTO);
	List<MemberIntegrateInfo> selectChannelProxy(MemberIntegrateInfo memberIntegrateInfo);
}

/*
 * 修改历史
 * $Log$ 
 */