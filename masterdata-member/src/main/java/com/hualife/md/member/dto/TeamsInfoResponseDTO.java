/**
 * This class is generated by the generator.
 * Any question, please contact to sunxiaodong(13401146791).
 */

package com.hualife.md.member.dto;

/**
 * auto generate TeamsInfoResponseDTO model
 * 
 * @author generator
 * @date 2017-11-22
 */
public class TeamsInfoResponseDTO {
	private String teamId;
	private String teamName;

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

}