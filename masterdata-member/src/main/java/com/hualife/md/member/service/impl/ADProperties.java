/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/


package com.hualife.md.member.service.impl;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public enum ADProperties {

	url, whenChanged, employeeID, name, userPrincipalName, physicalDeliveryOfficeName, 
	departmentNumber, telephoneNumber, homePhone, mobile, department, sAMAccountName, 
	mail,company,distinguishedName,instanceType,uSNCreated, uSNChanged,objectGUID,
	userAccountControl,badPwdCount,codePage,countryCode,badPasswordTime,lastLogoff,
	lastLogon,pwdLastSet,primaryGroupID,objectSid,accountExpires,logonCount,sAMAccountType,
	objectCategory,nTSecurityDescriptor,cn,sn,c,l,st,title,description,postalCode,
	postOfficeBox,facsimileTelephoneNumber,givenName,initials,whenCreated,displayName,
	otherTelephone,co,streetAddress,otherHomePhone,wWWHomePage,ipPhone,otherIpPhone,
	otherMobile,otherFacsimileTelephoneNumber,pager,otherPager,memberOf,userpassword;

	public static String[] getAttrNames() {
		ADProperties[] adProperties = ADProperties.values();
		String[] attrNames = new String[adProperties.length];
		int i = 0;
		for (ADProperties adProperty : adProperties) {
			attrNames[i] = adProperty.name();
			i++;
		}
		return attrNames;

	}

}

/*
 * 修改历史
 * $Log$ 
 */