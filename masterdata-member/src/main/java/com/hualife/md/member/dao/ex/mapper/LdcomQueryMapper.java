/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年11月17日
 *******************************************************************************/


package com.hualife.md.member.dao.ex.mapper;

import java.util.concurrent.CopyOnWriteArrayList;

import com.hualife.md.member.dao.mybatis.entity.SynLisLdcom;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface LdcomQueryMapper {
	CopyOnWriteArrayList<SynLisLdcom> selectUnIntegratedLdcom();
}

/*
 * 修改历史
 * $Log$ 
 */