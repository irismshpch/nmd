/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/


package com.hualife.md.member.service;

import java.util.List;

import com.hualife.md.member.dao.ex.entity.MemberIntegrateInfo;
import com.hualife.md.member.dao.mybatis.entity.MemberInfo;
import com.hualife.md.member.dao.mybatis.entity.OrganizationInfo;
import com.hualife.md.member.dao.mybatis.entity.OrganizationalunitInfo;
import com.hualife.md.member.dao.mybatis.entity.SynLisLaagent;
import com.hualife.md.member.dao.mybatis.entity.SynLisLdcom;
import com.hualife.support.util.PageDTO;


/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IMemberQueryService {
	public MemberIntegrateInfo queryMemberInfo(MemberInfo info);
	
	public MemberIntegrateInfo queryGatherMemberInfo(MemberInfo info);

	public List<MemberInfo> queryMemberInfos(MemberInfo info);
	
	public SynLisLdcom querySynLisLdcom(SynLisLdcom ldcom);

	public OrganizationInfo queryOrganizationInfo(OrganizationInfo organizationInfo);
	
	public List<OrganizationInfo> querySubOrganizationInfos(OrganizationInfo organizationInfo);
	
	public int queryTotalMembersByAffiliation(String affiliationCode);
	
	public List<SynLisLaagent> queryLaagentInfos(SynLisLaagent info);
	
	public List<MemberIntegrateInfo> queryMemberIntegrateInfos(String searchTerms, String searchOperator);
	
	public List<MemberIntegrateInfo> queryMemberIntegrateInfos(String searchTerms, String chnId, String searchOperator);
	
	public List<OrganizationInfo> queryOrganizationInfo(String wholeOrgCode);
	
	public List<OrganizationalunitInfo> queryOrganizationalunitInfo(String wholeOrgCode);
	
	public List<OrganizationInfo> querySubOrganizationInfos(String wholeOrgCode);
	
	public List<OrganizationalunitInfo> querySubOrganizationalunitInfos(String wholeOrgCode);
	
	public List<OrganizationInfo> querySubOrganizationInfos(String wholeOrgCode, String chnId);
	
	public int queryTotalLaagentsByInside(String wholeOrgCode, String chnId);
	
	public List<MemberIntegrateInfo> queryMemberInfos(String wholeOrgCode, String chnId, String level, List<String> gradeList, PageDTO pageDTO);
	
	public int queryTotalMemberInfos(String wholeOrgCode, String chnId, String level, List<String> gradeList, PageDTO pageDTO);
	
	public List<MemberIntegrateInfo> queryGatherMemberInfos(String wholeOrgCode, String level, PageDTO page);
	
	public int queryTotalGatherMemberInfos(String wholeOrgCode, String level, PageDTO pageDTO);
	
	public List<MemberIntegrateInfo> queryChannelProxy(String memberId);
}

/*
 * 修改历史
 * $Log$ 
 */