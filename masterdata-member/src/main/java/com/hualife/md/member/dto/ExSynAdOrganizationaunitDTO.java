package com.hualife.md.member.dto;

public class ExSynAdOrganizationaunitDTO {
	private String ouId;
	private String ouName;
	private String wholeOuName;
	private String affiliationOuName;
	private String pid;
	private Integer level;
	private Long weight;
	private Integer totalNum;
	
	public ExSynAdOrganizationaunitDTO() {
		super();
	}

	

	public ExSynAdOrganizationaunitDTO(String ouId, String ouName, String wholeOuName, String affiliationOuName,
			String pid, Long weight, Integer totalNum) {
		super();
		this.ouId = ouId;
		this.ouName = ouName;
		this.wholeOuName = wholeOuName;
		this.affiliationOuName = affiliationOuName;
		this.pid = pid;
		this.weight = weight;
		this.totalNum = totalNum;
	}



	public String getOuId() {
		return ouId;
	}

	public void setOuId(String ouId) {
		this.ouId = ouId;
	}

	public String getOuName() {
		return ouName;
	}

	public void setOdName(String odName) {
		this.ouName = odName;
	}

	public String getWholeOuName() {
		return wholeOuName;
	}

	public void setWholeOuName(String wholeOuName) {
		this.wholeOuName = wholeOuName;
	}

	public String getAffiliationOuName() {
		return affiliationOuName;
	}

	public void setAffiliationOuName(String affiliationOuName) {
		this.affiliationOuName = affiliationOuName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}



	public Long getWeight() {
		return weight;
	}



	public void setWeight(Long weight) {
		this.weight = weight;
	}



	public Integer getTotalNum() {
		return totalNum;
	}



	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}



	public void setOuName(String ouName) {
		this.ouName = ouName;
	}



	/**
	 * @return Returns the level.
	 */
	public Integer getLevel() {
		return level;
	}



	/**
	 * @param level The level to set.
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	

	
	
	
}
