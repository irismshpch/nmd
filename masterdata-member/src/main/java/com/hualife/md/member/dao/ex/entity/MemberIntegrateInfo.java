/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年11月22日
 *******************************************************************************/

package com.hualife.md.member.dao.ex.entity;

import java.util.List;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class MemberIntegrateInfo {
	private String memberId;
	private String memberName;
	private String organizationUnit;
	private String organizationCode;
	private String organizationName;
	private String organizationShortname;
	private String affiliationCode;
	private String affiliationName;
	private String orgunitCode;
	private String orgunitName;
	private String affiorgunitCode;
	private String affiorgunitName;
	private String email;
	private String state;
	private String department;
	private String title;
	private String channel;
	private String channelName;
	private String grade;
	private String gradeName;
	private List<String> channelProxy;
	private List<String> gradeList;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getOrganizationUnit() {
		return organizationUnit;
	}

	public void setOrganizationUnit(String organizationUnit) {
		this.organizationUnit = organizationUnit;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOrganizationShortname() {
		return organizationShortname;
	}

	public void setOrganizationShortname(String organizationShortname) {
		this.organizationShortname = organizationShortname;
	}

	public String getAffiliationCode() {
		return affiliationCode;
	}

	public void setAffiliationCode(String affiliationCode) {
		this.affiliationCode = affiliationCode;
	}

	public String getAffiliationName() {
		return affiliationName;
	}

	public void setAffiliationName(String affiliationName) {
		this.affiliationName = affiliationName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the channel.
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel The channel to set.
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @return Returns the channelName.
	 */
	public String getChannelName() {
		return channelName;
	}

	/**
	 * @param channelName The channelName to set.
	 */
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	/**
	 * @return Returns the grade.
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade The grade to set.
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return Returns the gradeName.
	 */
	public String getGradeName() {
		return gradeName;
	}

	/**
	 * @param gradeName The gradeName to set.
	 */
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	/**
	 * @return Returns the channelProxy.
	 */
	public List<String> getChannelProxy() {
		return channelProxy;
	}

	/**
	 * @param channelProxy The channelProxy to set.
	 */
	public void setChannelProxy(List<String> channelProxy) {
		this.channelProxy = channelProxy;
	}

	/**
	 * @return Returns the orgunitCode.
	 */
	public String getOrgunitCode() {
		return orgunitCode;
	}

	/**
	 * @param orgunitCode The orgunitCode to set.
	 */
	public void setOrgunitCode(String orgunitCode) {
		this.orgunitCode = orgunitCode;
	}

	/**
	 * @return Returns the orgunitName.
	 */
	public String getOrgunitName() {
		return orgunitName;
	}

	/**
	 * @param orgunitName The orgunitName to set.
	 */
	public void setOrgunitName(String orgunitName) {
		this.orgunitName = orgunitName;
	}

	/**
	 * @return Returns the affiorgunitCode.
	 */
	public String getAffiorgunitCode() {
		return affiorgunitCode;
	}

	/**
	 * @param affiorgunitCode The affiorgunitCode to set.
	 */
	public void setAffiorgunitCode(String affiorgunitCode) {
		this.affiorgunitCode = affiorgunitCode;
	}

	/**
	 * @return Returns the affiorgunitName.
	 */
	public String getAffiorgunitName() {
		return affiorgunitName;
	}

	/**
	 * @param affiorgunitName The affiorgunitName to set.
	 */
	public void setAffiorgunitName(String affiorgunitName) {
		this.affiorgunitName = affiorgunitName;
	}

	/**
	 * @return Returns the gradeList.
	 */
	public List<String> getGradeList() {
		return gradeList;
	}

	/**
	 * @param gradeList The gradeList to set.
	 */
	public void setGradeList(List<String> gradeList) {
		this.gradeList = gradeList;
	}

}

/*
 * 修改历史 $Log$
 */