/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年11月30日
 *******************************************************************************/


package com.hualife.md.member.dto.organization;

import java.util.List;

/**
 * @description: 组织架构bean，递归
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public class OrgAgentDTO {
	private String code;
	private String name;
	private int num;
	private List<AgentsInfoDTO> mbr;
	private List<OrgAgentDTO> subOrg;

	/**
	 * @return Returns the num.
	 */
	public int getNum() {
		return num;
	}

	/**
	 * @param num The num to set.
	 */
	public void setNum(int num) {
		this.num = num;
	}


	/**
	 * @return Returns the mbr.
	 */
	public List<AgentsInfoDTO> getMbr() {
		return mbr;
	}

	/**
	 * @param mbr The mbr to set.
	 */
	public void setMbr(List<AgentsInfoDTO> mbr) {
		this.mbr = mbr;
	}

	/**
	 * @return Returns the subOrg.
	 */
	public List<OrgAgentDTO> getSubOrg() {
		return subOrg;
	}

	/**
	 * @param subOrg The subOrg to set.
	 */
	public void setSubOrg(List<OrgAgentDTO> subOrg) {
		this.subOrg = subOrg;
	}

	/**
	 * @return Returns the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code The code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}


}

/*
 * 修改历史
 * $Log$ 
 */