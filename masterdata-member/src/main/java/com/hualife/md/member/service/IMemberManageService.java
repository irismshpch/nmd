/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年5月31日
 *******************************************************************************/

package com.hualife.md.member.service;

import com.hualife.md.member.dao.mybatis.entity.MemberInfo;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IMemberManageService {
	public MemberInfo login(String memberId, String password);
}

/*
 * 修改历史 $Log$
 */