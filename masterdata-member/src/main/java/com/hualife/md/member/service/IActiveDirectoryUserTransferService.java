/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/


package com.hualife.md.member.service;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IActiveDirectoryUserTransferService {
	public void transfer();
	
	public void transferOU();
}

/*
 * 修改历史
 * $Log$ 
 */