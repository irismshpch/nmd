/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年9月20日
 *******************************************************************************/

package com.hualife.md.member.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hualife.md.member.dao.ex.entity.MemberIntegrateInfo;
import com.hualife.md.member.dao.ex.mapper.MemberIntegrateInfoQueryMapper;
import com.hualife.md.member.dao.mybatis.entity.MemberInfo;
import com.hualife.md.member.dao.mybatis.entity.MemberInfoExample;
import com.hualife.md.member.dao.mybatis.entity.OrganizationInfo;
import com.hualife.md.member.dao.mybatis.entity.OrganizationInfoExample;
import com.hualife.md.member.dao.mybatis.entity.OrganizationalunitInfo;
import com.hualife.md.member.dao.mybatis.entity.OrganizationalunitInfoExample;
import com.hualife.md.member.dao.mybatis.entity.SynLisLaagent;
import com.hualife.md.member.dao.mybatis.entity.SynLisLaagentExample;
import com.hualife.md.member.dao.mybatis.entity.SynLisLdcom;
import com.hualife.md.member.dao.mybatis.mapper.MemberInfoMapper;
import com.hualife.md.member.dao.mybatis.mapper.OrganizationInfoMapper;
import com.hualife.md.member.dao.mybatis.mapper.OrganizationalunitInfoMapper;
import com.hualife.md.member.dao.mybatis.mapper.SynLisLaagentMapper;
import com.hualife.md.member.dao.mybatis.mapper.SynLisLdcomMapper;
import com.hualife.md.member.service.IMemberQueryService;
import com.hualife.support.exception.BusinessException;
import com.hualife.support.util.PageDTO;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("memberQueryService")
public class MemberQueryServiceImpl implements IMemberQueryService {
	@Autowired
	private MemberInfoMapper memberInfoMapper;

	@Autowired
	private MemberIntegrateInfoQueryMapper memberIntegrateInfoQueryMapper;

	@Autowired
	private SynLisLdcomMapper synLisLdcomMapper;

	@Autowired
	private SynLisLaagentMapper synLisLaagentMapper;

	@Autowired
	private OrganizationInfoMapper organizationInfoMapper;

	@Autowired
	private OrganizationalunitInfoMapper organizationalunitInfoMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryMemberInfo(com
	 * .hualife.md.member.dao.ex.entity.MemberIntegrateInfo)
	 */
	@Override
	public MemberIntegrateInfo queryMemberInfo(MemberInfo info) {
		if (info == null || info.getMemberId() == null
				|| "".equals(info.getMemberId().trim()))
			throw new BusinessException("查询条件不能为空！");
		MemberIntegrateInfo memberIntegrateInfo = memberIntegrateInfoQueryMapper
				.selectMemberInfoById(info.getMemberId().trim());
		if (memberIntegrateInfo == null)
			memberIntegrateInfo = memberIntegrateInfoQueryMapper
					.selectLaagentInfoById(info.getMemberId().trim());
		return memberIntegrateInfo;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryMemberInfos(com
	 * .hualife.md.member.dao.mybatis.entity.MemberInfo)
	 */
	@Override
	public List<MemberInfo> queryMemberInfos(MemberInfo info) {
		if (info == null || info.getOrganizationCode() == null
				|| "".equals(info.getOrganizationCode().trim()))
			throw new BusinessException("查询条件不能为空！");
		MemberInfoExample example = new MemberInfoExample();
		MemberInfoExample.Criteria criteria = example.createCriteria();
		criteria.andOrganizationCodeEqualTo(info.getOrganizationCode().trim());
		return memberInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#querySynLisLdcom(com
	 * .hualife.md.member.dao.mybatis.entity.SynLisLdcom)
	 */
	@Override
	public SynLisLdcom querySynLisLdcom(SynLisLdcom ldcom) {
		if (ldcom == null || ldcom.getComcode() == null
				|| "".equals(ldcom.getComcode().trim()))
			throw new BusinessException("机构编码不能为空！");
		return synLisLdcomMapper.selectByPrimaryKey(ldcom.getComcode().trim());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryOrganizationInfo
	 * (com.hualife.md.member.dao.mybatis.entity.OrganizationInfo)
	 */
	@Override
	public OrganizationInfo queryOrganizationInfo(
			OrganizationInfo organizationInfo) {
		if (organizationInfo == null
				|| organizationInfo.getOrganizationCode() == null
				|| "".equals(organizationInfo.getOrganizationCode().trim()))
			throw new BusinessException("机构编码不能为空！");
		return organizationInfoMapper.selectByPrimaryKey(organizationInfo
				.getOrganizationCode().trim());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryOrganizationInfos
	 * (com.hualife.md.member.dao.mybatis.entity.OrganizationInfo)
	 */
	@Override
	public List<OrganizationInfo> querySubOrganizationInfos(
			OrganizationInfo organizationInfo) {
		if (organizationInfo == null
				|| organizationInfo.getOrganizationCode() == null
				|| "".equals(organizationInfo.getOrganizationCode().trim()))
			throw new BusinessException("机构编码不能为空！");
		if (organizationInfo.getAffiliationCode() == null
				|| "".equals(organizationInfo.getAffiliationCode().trim())) {
			organizationInfo = organizationInfoMapper
					.selectByPrimaryKey(organizationInfo.getOrganizationCode()
							.trim());
			if (organizationInfo == null)
				return null;
		}
		OrganizationInfoExample example = new OrganizationInfoExample();
		OrganizationInfoExample.Criteria criteria = example.createCriteria();
		criteria.andAffiliationCodeEqualTo(organizationInfo
				.getWholeOrganizationCode());
		return organizationInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hualife.md.member.service.IMemberQueryService#
	 * queryTotalMembersByAffiliation(java.lang.String)
	 */
	@Override
	public int queryTotalMembersByAffiliation(String affiliationCode) {
		if (affiliationCode == null || "".equals(affiliationCode.trim()))
			throw new BusinessException("查询条件不能为空！");
		return memberIntegrateInfoQueryMapper
				.selectTotalMembersByAffiliation(affiliationCode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryLaagentInfos(com
	 * .hualife.md.member.dao.mybatis.entity.SynLisLaagent)
	 */
	@Override
	public List<SynLisLaagent> queryLaagentInfos(SynLisLaagent info) {
		if (info == null || info.getAgentgroup() == null
				|| "".equals(info.getAgentgroup().trim()))
			throw new BusinessException("查询条件不能为空！");
		SynLisLaagentExample example = new SynLisLaagentExample();
		SynLisLaagentExample.Criteria criteria = example.createCriteria();
		criteria.andAgentgroupEqualTo(info.getAgentgroup().trim());
		List<String> state = new ArrayList<String>();
		state.add("01");
		state.add("02");
		criteria.andAgentstateIn(state);
		return synLisLaagentMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryMemberIntegrateInfos
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public List<MemberIntegrateInfo> queryMemberIntegrateInfos(
			String searchTerms, String searchOperator) {
		if (searchOperator == null || "".equals(searchOperator.trim()))
			throw new BusinessException("检索人不能为空！");
		if (searchTerms == null || "".equals(searchTerms.trim()))
			throw new BusinessException("检索条件不能为空！");
		MemberInfo member = new MemberInfo();
		member.setMemberId(searchOperator.trim());
		MemberIntegrateInfo searchOperatorInfo = this.queryMemberInfo(member);
		if (searchOperatorInfo == null) {
			throw new BusinessException("检索人无检索权限！");
		}
		MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
		queryInfo.setMemberName(searchTerms.trim());
		queryInfo.setAffiliationCode(searchOperatorInfo.getAffiliationCode());
		return memberIntegrateInfoQueryMapper.selectMembersByName(queryInfo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryOrganizationInfo
	 * (java.lang.String)
	 */
	@Override
	public List<OrganizationInfo> queryOrganizationInfo(String wholeOrgCode) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim()))
			throw new BusinessException("机构编码不能为空！");
		OrganizationInfoExample example = new OrganizationInfoExample();
		OrganizationInfoExample.Criteria criteria = example.createCriteria();
		criteria.andWholeOrganizationCodeEqualTo(wholeOrgCode.trim().trim());
		return organizationInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryMemberInfos(java
	 * .lang.String, java.lang.String, java.lang.String, java.util.List,
	 * com.hualife.support.util.PageDTO)
	 */
	@Override
	public List<MemberIntegrateInfo> queryMemberInfos(String wholeOrgCode,
			String chnId, String level, List<String> gradeList, PageDTO pageDTO) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim())
				|| chnId == null || "".equals(chnId.trim()))
			throw new BusinessException("机构编码、渠道编码不能为空！");
		MemberIntegrateInfo memberIntegrateInfo = new MemberIntegrateInfo();
		memberIntegrateInfo.setAffiliationCode(wholeOrgCode.trim());
		memberIntegrateInfo.setChannel(chnId.trim());
		memberIntegrateInfo.setGradeList(gradeList);
		if ("0".equals(chnId.trim())) {
			if ("Y".equals(level)) {
				return memberIntegrateInfoQueryMapper
						.selectMemberInfosByLevel(memberIntegrateInfo);
			} else {
				if (pageDTO == null)
					return memberIntegrateInfoQueryMapper
							.selectMemberInfosByTerms(pageDTO);
				else {
					pageDTO.setParameters(memberIntegrateInfo);
					return memberIntegrateInfoQueryMapper
							.selectMemberInfosByTermsPage(pageDTO);
				}
			}

		} else {
			if (pageDTO == null) {
				pageDTO = new PageDTO();
				pageDTO.setParameters(memberIntegrateInfo);
				return memberIntegrateInfoQueryMapper
						.selectLaagentInfosByTerms(pageDTO);
			} else {
				pageDTO.setParameters(memberIntegrateInfo);
				return memberIntegrateInfoQueryMapper
						.selectLaagentInfosByTermsPage(pageDTO);
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryTotalMemberInfos
	 * (java.lang.String, java.lang.String, java.lang.String, java.util.List,
	 * com.hualife.support.util.PageDTO)
	 */
	@Override
	public int queryTotalMemberInfos(String wholeOrgCode, String chnId,
			String level, List<String> gradeList, PageDTO pageDTO) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim())
				|| chnId == null || "".equals(chnId.trim()))
			throw new BusinessException("机构编码、渠道编码不能为空！");
		MemberIntegrateInfo memberIntegrateInfo = new MemberIntegrateInfo();
		memberIntegrateInfo.setAffiliationCode(wholeOrgCode.trim());
		memberIntegrateInfo.setChannel(chnId.trim());
		memberIntegrateInfo.setGradeList(gradeList);
		if ("0".equals(chnId.trim())) {
			if (pageDTO == null) {
				pageDTO = new PageDTO();
			}
			pageDTO.setParameters(memberIntegrateInfo);
			return memberIntegrateInfoQueryMapper
					.selectTotalMemberInfosByTermsPage(pageDTO);
		} else {
			if (pageDTO == null) {
				pageDTO = new PageDTO();
			}
			pageDTO.setParameters(memberIntegrateInfo);
			return memberIntegrateInfoQueryMapper
					.selectTotalLaagentInfosByTermsPage(pageDTO);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#querySubOrganizationInfos
	 * (java.lang.String)
	 */
	@Override
	public List<OrganizationInfo> querySubOrganizationInfos(String wholeOrgCode) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim()))
			throw new BusinessException("机构编码不能为空！");
		OrganizationInfoExample example = new OrganizationInfoExample();
		OrganizationInfoExample.Criteria criteria = example.createCriteria();
		criteria.andAffiliationCodeEqualTo(wholeOrgCode.trim());
		return organizationInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#querySubOrganizationInfos
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public List<OrganizationInfo> querySubOrganizationInfos(
			String wholeOrgCode, String chnId) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim())
				|| chnId == null || "".equals(chnId.trim()))
			throw new BusinessException("机构编码、渠道编码不能为空！");
		OrganizationInfoExample example = new OrganizationInfoExample();
		OrganizationInfoExample.Criteria criteria = example.createCriteria();
		criteria.andAffiliationCodeEqualTo(wholeOrgCode.trim());
		criteria.andChannelEqualTo(chnId.trim());
		return organizationInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryTotalLaagentsByInside
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public int queryTotalLaagentsByInside(String wholeOrgCode, String chnId) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim())
				|| chnId == null || "".equals(chnId.trim()))
			throw new BusinessException("机构编码、渠道编码不能为空！");
		MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
		queryInfo.setAffiliationCode(wholeOrgCode);
		queryInfo.setChannel(chnId);
		return memberIntegrateInfoQueryMapper
				.selectTotalLaagentsByInside(queryInfo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryMemberIntegrateInfos
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<MemberIntegrateInfo> queryMemberIntegrateInfos(
			String searchTerms, String chnId, String searchOperator) {
		if (searchOperator == null || "".equals(searchOperator.trim()))
			throw new BusinessException("检索人不能为空！");

		if (searchTerms == null || "".equals(searchTerms.trim()))
			throw new BusinessException("检索条件不能为空！");
		MemberInfo member = new MemberInfo();
		member.setMemberId(searchOperator.trim());
		MemberIntegrateInfo searchOperatorInfo = this
				.queryGatherMemberInfo(member);
		if (searchOperatorInfo == null) {
			throw new BusinessException("检索人无检索权限！");
		}

		if (chnId != null && !"".equals(chnId.trim())) { // 检索渠道为空时，先校验其权限，再进行搜索
			if ("0".equals(searchOperatorInfo.getChannel())) { // 如果是内勤
				if (!chnId.equals(searchOperatorInfo.getChannel())) { // 如果检索的渠道不是内勤渠道
					if (searchOperatorInfo.getAffiliationCode() != null
							&& !"".equals(searchOperatorInfo
									.getAffiliationCode().trim())) { // 内勤用户的机构编码为空时，不允许检索其他渠道
						throw new BusinessException("检索人无该渠道的检索权限！");
					}
					MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
					queryInfo.setMemberId(searchOperator.trim());
					List<String> channelProxy = new ArrayList<String>();
					channelProxy.add(chnId);
					queryInfo.setChannelProxy(channelProxy);
					List<MemberIntegrateInfo> checkResult = memberIntegrateInfoQueryMapper
							.selectChannelProxy(queryInfo);
					if (checkResult == null || checkResult.size() < 1) {
						throw new BusinessException("检索人无该渠道的检索权限！");
					}
					queryInfo.setMemberName(searchTerms.trim());
					queryInfo.setAffiliationCode(searchOperatorInfo
							.getAffiliationCode());
					return memberIntegrateInfoQueryMapper
							.selectLaagentsByName(queryInfo); // 查询外勤
				} else {
					MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
					queryInfo.setMemberName(searchTerms.trim());
					// queryInfo.setAffiliationCode(searchOperatorInfo
					// .getAffiliationCode());
					return memberIntegrateInfoQueryMapper
							.selectGatherMembersByName(queryInfo); // 查询内勤
				}

			} else { // 如果是外勤
				if (!chnId.equals(searchOperatorInfo.getChannel())) { // 渠道不一致，直接报错
					throw new BusinessException("检索人无该渠道的检索权限！");
				} else {
					MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
					queryInfo.setMemberName(searchTerms.trim());
					queryInfo.setAffiliationCode(searchOperatorInfo
							.getAffiliationCode());
					return memberIntegrateInfoQueryMapper
							.selectLaagentsByName(queryInfo); // 查询外勤
				}

			}

		} else { // 检索渠道为空时，先判断其权限，再进行搜索
			if ("0".equals(searchOperatorInfo.getChannel())) { // 如果是内勤

				MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
				queryInfo.setMemberName(searchTerms.trim());
				// queryInfo.setAffiliationCode(searchOperatorInfo
				// .getAffiliationCode());
				List<MemberIntegrateInfo> result = memberIntegrateInfoQueryMapper
						.selectGatherMembersByName(queryInfo); // 查询内勤
				// 判断有无外勤权限，如果有，则继续查询外勤
				List<MemberIntegrateInfo> channelProxyResultList = this
						.queryChannelProxy(searchOperator.trim());
				if (channelProxyResultList != null
						&& channelProxyResultList.size() > 0) {
					MemberIntegrateInfo queryInfo2 = new MemberIntegrateInfo();
					queryInfo2.setMemberName(searchTerms.trim());
					queryInfo2.setAffiliationCode(searchOperatorInfo
							.getAffiliationCode());

					List<String> channelProxy = new ArrayList<String>();
					for (MemberIntegrateInfo channelProxyResult : channelProxyResultList) {
						channelProxy.add(channelProxyResult.getChannel());
					}
					queryInfo2.setChannelProxy(channelProxy);
					List<MemberIntegrateInfo> result2 = memberIntegrateInfoQueryMapper
							.selectLaagentsByName(queryInfo2); // 查询外勤
					if (result2 != null && result2.size() > 0) {
						result.addAll(result2);
					}
				}

				return result;
			} else { // 如果是外勤，直接检索名下外勤
				MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
				queryInfo.setMemberName(searchTerms.trim());
				queryInfo.setAffiliationCode(searchOperatorInfo
						.getAffiliationCode());
				return memberIntegrateInfoQueryMapper
						.selectLaagentsByName(queryInfo); // 查询外勤
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryChannelProxy(java
	 * .lang.String)
	 */
	@Override
	public List<MemberIntegrateInfo> queryChannelProxy(String memberId) {
		if (memberId == null || "".equals(memberId.trim()))
			throw new BusinessException("用户编码不能为空！");
		MemberIntegrateInfo queryInfo = new MemberIntegrateInfo();
		queryInfo.setMemberId(memberId.trim());
		return memberIntegrateInfoQueryMapper.selectChannelProxy(queryInfo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryGatherMemberInfo
	 * (com.hualife.md.member.dao.mybatis.entity.MemberInfo)
	 */
	@Override
	public MemberIntegrateInfo queryGatherMemberInfo(MemberInfo info) {
		if (info == null || info.getMemberId() == null
				|| "".equals(info.getMemberId().trim()))
			throw new BusinessException("查询条件不能为空！");
		MemberIntegrateInfo memberIntegrateInfo = memberIntegrateInfoQueryMapper
				.selectGatherMemberInfoById(info.getMemberId().trim());
		if (memberIntegrateInfo == null)
			memberIntegrateInfo = memberIntegrateInfoQueryMapper
					.selectLaagentInfoById(info.getMemberId().trim());
		return memberIntegrateInfo;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryOrganizationalunitInfo
	 * (java.lang.String)
	 */
	@Override
	public List<OrganizationalunitInfo> queryOrganizationalunitInfo(
			String wholeOrgCode) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim()))
			throw new BusinessException("机构编码不能为空！");
		OrganizationalunitInfoExample example = new OrganizationalunitInfoExample();
		OrganizationalunitInfoExample.Criteria criteria = example
				.createCriteria();
		criteria.andWholeOuIdEqualTo(wholeOrgCode.trim());
		return organizationalunitInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hualife.md.member.service.IMemberQueryService#
	 * querySubOrganizationalunitInfos(java.lang.String)
	 */
	@Override
	public List<OrganizationalunitInfo> querySubOrganizationalunitInfos(
			String wholeOrgCode) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim()))
			throw new BusinessException("机构编码不能为空！");
		OrganizationalunitInfoExample example = new OrganizationalunitInfoExample();
		OrganizationalunitInfoExample.Criteria criteria = example
				.createCriteria();
		criteria.andAffiliationOuIdEqualTo(wholeOrgCode.trim());
		example.setOrderByClause("weight asc");
		return organizationalunitInfoMapper.selectByExample(example);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.member.service.IMemberQueryService#queryGatherMemberInfos
	 * (java.lang.String, java.lang.String, com.hualife.support.util.PageDTO)
	 */
	@Override
	public List<MemberIntegrateInfo> queryGatherMemberInfos(
			String wholeOrgCode, String level, PageDTO pageDTO) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim()))
			throw new BusinessException("机构编码、渠道编码不能为空！");
		MemberIntegrateInfo memberIntegrateInfo = new MemberIntegrateInfo();
		memberIntegrateInfo.setAffiorgunitCode(wholeOrgCode.trim());
		if ("Y".equals(level))
			return memberIntegrateInfoQueryMapper
					.selectGatherMemberInfosByLevel(memberIntegrateInfo);
		else {
			if(pageDTO == null) {
				pageDTO = new PageDTO();
				pageDTO.setParameters(memberIntegrateInfo);
				return memberIntegrateInfoQueryMapper
						.selectGatherMemberInfosByTerms(pageDTO);
			}
			else {
				pageDTO.setParameters(memberIntegrateInfo);
				return memberIntegrateInfoQueryMapper
						.selectGatherMemberInfosByTermsPage(pageDTO);
			}
		}
			
	}

	/* (non-Javadoc)
	 * @see com.hualife.md.member.service.IMemberQueryService#queryTotalGatherMemberInfos(java.lang.String, java.lang.String, com.hualife.support.util.PageDTO)
	 */
	@Override
	public int queryTotalGatherMemberInfos(String wholeOrgCode, String level,
			PageDTO pageDTO) {
		if (wholeOrgCode == null || "".equals(wholeOrgCode.trim()))
			throw new BusinessException("机构编码、渠道编码不能为空！");
		MemberIntegrateInfo memberIntegrateInfo = new MemberIntegrateInfo();
		memberIntegrateInfo.setAffiorgunitCode(wholeOrgCode.trim());
		if(pageDTO == null) {
			pageDTO = new PageDTO();
		}
			pageDTO.setParameters(memberIntegrateInfo);
			return memberIntegrateInfoQueryMapper
					.selectTotalGatherMemberInfosByTermsPage(pageDTO);
	}

}

/*
 * 修改历史 $Log$
 */