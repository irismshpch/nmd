/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月17日
 *******************************************************************************/


package com.hualife.md.team.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hualife.md.team.dao.ex.entity.TeamMembersInfo;
import com.hualife.md.team.dao.ex.entity.TeamQuery;
import com.hualife.md.team.dao.ex.mapper.TeamMembersInfoQueryMapper;
import com.hualife.md.team.dao.mybatis.entity.TeamInfo;
import com.hualife.md.team.dao.mybatis.mapper.TeamInfoMapper;
import com.hualife.md.team.service.ITeamQueryService;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("teamQueryService")
public class TeamQueryServiceImpl implements ITeamQueryService {
	@Autowired
	private TeamInfoMapper teamInfoMapper;
	
	@Autowired
	private TeamMembersInfoQueryMapper teamMembersInfoQueryMapper;

	/* (non-Javadoc)
	 * @see com.hualife.md.team.service.ITeamQueryService#selectTeamInfo(java.lang.String)
	 */
	@Override
	public TeamInfo selectTeamInfo(String teamId) {
		return teamInfoMapper.selectByPrimaryKey(Integer.parseInt(teamId));
	}

	/* (non-Javadoc)
	 * @see com.hualife.md.team.service.ITeamQueryService#selectMembersInfoByTeamId(com.hualife.md.team.dao.ex.entity.TeamQuery)
	 */
	@Override
	public List<TeamMembersInfo> selectMembersInfoByTeamId(TeamQuery teamQuery) {
		return teamMembersInfoQueryMapper.selectMembersInfoByTeamId(teamQuery);
	}

	/* (non-Javadoc)
	 * @see com.hualife.md.team.service.ITeamQueryService#selectTotalMembersByTeamId(com.hualife.md.team.dao.ex.entity.TeamQuery)
	 */
	@Override
	public int selectTotalMembersByTeamId(TeamQuery teamQuery) {
		return teamMembersInfoQueryMapper.selectTotalMembersByTeamId(teamQuery);
	}

	/* (non-Javadoc)
	 * @see com.hualife.md.team.service.ITeamQueryService#selectTeamsInfoByMemberId(java.lang.String)
	 */
	@Override
	public List<TeamMembersInfo> selectTeamsInfoByMemberId(String memberId) {
		return teamMembersInfoQueryMapper.selectTeamsInfoByMemberId(memberId);
	}

}

/*
 * 修改历史
 * $Log$ 
 */