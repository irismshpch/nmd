/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月10日
 *******************************************************************************/

package com.hualife.md.team.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hualife.md.constants.APIConstants;
import com.hualife.md.team.dao.ex.entity.TeamMembersInfo;
import com.hualife.md.team.dao.ex.entity.TeamQuery;
import com.hualife.md.team.dto.lite.HeadImgResponseDTO;
import com.hualife.md.team.dto.lite.MbrTeamQueryRequestDTO;
import com.hualife.md.team.dto.lite.MbrTeamQueryResponseDTO;
import com.hualife.md.team.dto.lite.TeamResponseDTO;
import com.hualife.md.team.service.ITeamManageService;
import com.hualife.md.team.service.ITeamQueryService;
import com.hualife.support.exception.BusinessException;
import com.hualife.support.exception.SysException;
import com.hualife.support.util.ColorUtil;
import com.hualife.support.util.JsonUtil;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@RestController
@RequestMapping(value = "/api/v1")
public class TeamLiteController {
	private static final Logger logger = LoggerFactory
			.getLogger(TeamLiteController.class);

	@Autowired
	private ITeamQueryService teamQueryService;
	@Autowired
	private ITeamManageService teamManageService;

	@RequestMapping(value = "/mbr/team/query", method = RequestMethod.GET)
	public Map<String, Object> memberTeamQuery(HttpServletRequest request,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*"); // 解决前后端跨域问题
		String data = request.getParameter("data");
		if (data == null || "".equals(data.trim())) {
			throw new BusinessException("查询条件不允许为空！");
		}

		try {
			MbrTeamQueryRequestDTO requestDTO = (MbrTeamQueryRequestDTO) JsonUtil
					.SnakeJson2CamelBean(data, MbrTeamQueryRequestDTO.class);
			String mbrId = requestDTO.getMbrId();
			if (mbrId == null || "".equals(mbrId.trim())) {
				throw new BusinessException("查询条件不允许为空！");
			} else {
				mbrId = mbrId.trim();
			}
			List<TeamMembersInfo> teamMembersInfoList = teamQueryService.selectTeamsInfoByMemberId(mbrId);
			if (teamMembersInfoList == null || teamMembersInfoList.size() == 0) {
				throw new BusinessException("您尚未创建或加入群组！");
			}
			
			MbrTeamQueryResponseDTO responseDTO = new MbrTeamQueryResponseDTO();
			responseDTO.setMbrId(mbrId);
			List<TeamResponseDTO> teamList = new ArrayList<TeamResponseDTO>();
			for (TeamMembersInfo teamMembersInfo : teamMembersInfoList) {
				TeamResponseDTO team = new TeamResponseDTO();
				team.setTeamId(teamMembersInfo.getTeamId());
				team.setTeamName(teamMembersInfo.getTeamName());
				team.setMbrType(teamMembersInfo.getMemberType());
				TeamQuery teamQuery = new TeamQuery();
				teamQuery.setTeamId(teamMembersInfo.getTeamId());
				List<TeamMembersInfo> memberList = teamQueryService.selectMembersInfoByTeamId(teamQuery);
				int mbrNum = memberList.size();
				team.setMbrNum(mbrNum);
				
				
				List<HeadImgResponseDTO> headImgList = new ArrayList<HeadImgResponseDTO>();
				if (mbrNum <= 3) {
					for(TeamMembersInfo member:memberList) {
						HeadImgResponseDTO headImg = new HeadImgResponseDTO();
						String lastName = (member.getMemberName().length() == 1) ? member.getMemberName() : member.getMemberName().substring(1, 2);
						headImg.setLastName(lastName);
						headImg.setBgColor(ColorUtil.getColor(lastName));
						headImgList.add(headImg);
					}
					for (int i = 0; i < 3 - mbrNum; i++) {
						HeadImgResponseDTO headImg = new HeadImgResponseDTO();
						String lastName = "无";
						headImg.setLastName(lastName);
						headImg.setBgColor(ColorUtil.getColor(lastName));
						headImgList.add(headImg);
					}
				} else {
					memberList = memberList.subList(0, 4);
					for(TeamMembersInfo member:memberList) {
						HeadImgResponseDTO headImg = new HeadImgResponseDTO();
						String lastName = (member.getMemberName().length() == 1) ? member.getMemberName() : member.getMemberName().substring(1, 2);
						headImg.setLastName(lastName);
						headImg.setBgColor(ColorUtil.getColor(lastName));
						headImgList.add(headImg);
					}
				}
				
				team.setHeadImg(headImgList);
				
				teamList.add(team);
			}
			responseDTO.setTeam(teamList);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("result_code", APIConstants.RESULT_SUCCESS);
			map.put("result_message", "查询成功！");
			map.put("data", JsonUtil.CamelBean2SnakeJson(responseDTO));
			return map;
		} catch (JsonParseException e) {
			e.printStackTrace();
			logger.error("/api/v1/team/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			logger.error("/api/v1/team/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("/api/v1/team/query交易处理失败，原因是【{}】.", e.getMessage());
			throw new SysException(e.getMessage());
		}

	}
	
	
}

/*
 * 修改历史 $Log$
 */