/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年2月6日
 *******************************************************************************/

package com.hualife.md.team.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hualife.md.common.service.IWeChatGatewayService;
import com.hualife.md.common.service.IWeChatTagService;
import com.hualife.md.team.dao.ex.entity.MemberTeamRelaEx;
import com.hualife.md.team.dao.ex.mapper.MemberTeamRelaExMapper;
import com.hualife.md.team.dao.mybatis.entity.MemberTeamRela;
import com.hualife.md.team.dao.mybatis.entity.TeamInfo;
import com.hualife.md.team.dao.mybatis.entity.TeamInfoExample;
import com.hualife.md.team.dao.mybatis.mapper.MemberTeamRelaMapper;
import com.hualife.md.team.dao.mybatis.mapper.TeamInfoMapper;
import com.hualife.md.team.service.IWechatUserSynService;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("wechatUserSynService")
public class WechatUserSynServiceImpl implements IWechatUserSynService {
	private static final Logger logger = LoggerFactory
			.getLogger(WechatUserSynServiceImpl.class);
	@Autowired
	private TeamInfoMapper teamInfoMapper;
	@Autowired
	private MemberTeamRelaMapper memberTeamRelaMapper;
	@Autowired
	private MemberTeamRelaExMapper memberTeamRelaExMapper;

	@Autowired
	private IWeChatGatewayService weChatGatewayService;

	@Autowired
	private IWeChatTagService weChatTagService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hualife.md.team.service.IWechatUserSynService#syn2Wechat()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void syn2Wechat() {
		TeamInfoExample example = new TeamInfoExample();
		TeamInfoExample.Criteria criteria = example.createCriteria();
		criteria.andGroupIdIsNotNull();
		criteria.andTagIdIsNotNull();
		List<TeamInfo> teamInfoList = teamInfoMapper.selectByExample(example);
		String token = weChatGatewayService.getToken();
		for (TeamInfo teamInfo : teamInfoList) {
			try {
				List<String> tmsUserList = memberTeamRelaExMapper
						.selectTmsTeamInfos(teamInfo.getGroupId());
				if (tmsUserList == null || tmsUserList.size() == 0) {
					// 查不到数据时，默认为群组数据存在问题，跳过本次处理
					continue;
				}
				List<String> tmsUserCopyList = new ArrayList<String>();
				tmsUserCopyList.addAll(tmsUserList);
				String tagId = teamInfo.getTagId() + "";

				List<String> wechartUserList = weChatTagService.queryTagUser(
						tagId, token);

				if (wechartUserList == null)
					continue;
				else {

					for (String tmsUserId : tmsUserCopyList) {
						List<String> wechartUserCopyList = new ArrayList<String>();
						wechartUserCopyList.addAll(wechartUserList);
						for (String wechartUserid : wechartUserCopyList) {
							if (wechartUserid.equals(tmsUserId)) {
								tmsUserList.remove(tmsUserId);
								wechartUserList.remove(wechartUserid);
							}
						}
					}

					try {
						if (wechartUserList == null
								|| wechartUserList.size() == 0) {
						} else {
							List<String> list = new ArrayList<String>();
							for (String wechartUserId : wechartUserList) {
								list.add(wechartUserId);
							}
							logger.debug("需删除的人员信息：" + list);
							Map<String, Object> paraMap = new HashMap<String, Object>();
							paraMap.put("tagid", tagId);
							paraMap.put("userlist", list);
							weChatTagService.deleteTagUser(token, paraMap);
						}

					} catch (Throwable e) {

						e.printStackTrace();
					}
				}
				try {
					if (tmsUserList == null || tmsUserList.size() == 0) {
					} else {
						logger.debug("需添加的人员信息：" + tmsUserList);
						Map<String, Object> paraMap = new HashMap<String, Object>();
						paraMap.put("tagid", tagId);
						paraMap.put("userlist", tmsUserList);
						weChatTagService.addTagUser(token, paraMap);
					}

				} catch (Throwable e) {

					e.printStackTrace();
				}

			} catch (Throwable e) {
				e.printStackTrace();
			}

		}
	}

	@Override
	public void syn2MD() {

		TeamInfoExample example = new TeamInfoExample();
		TeamInfoExample.Criteria criteria = example.createCriteria();
		criteria.andGroupIdIsNotNull();
		criteria.andTagIdIsNotNull();
		List<TeamInfo> teamInfoList = teamInfoMapper.selectByExample(example);
		for (TeamInfo teamInfo : teamInfoList) {
			List<MemberTeamRelaEx> memberTeamRelaExList = memberTeamRelaExMapper
					.selectDiffMemberTeamInfos(teamInfo);
			for (MemberTeamRelaEx memberTeamRelaEx : memberTeamRelaExList) {

				try {
					if ("D".equals(memberTeamRelaEx.getOperate())) {
						memberTeamRelaMapper
								.deleteByPrimaryKey(memberTeamRelaEx.getId());
					} else if ("A".equals(memberTeamRelaEx.getOperate())) {
						MemberTeamRela record = new MemberTeamRela();
						record.setMemberId(memberTeamRelaEx.getMemberId());
						record.setTeamId(teamInfo.getTeamId());
						record.setCreateDate(new Date());
						record.setCreateUser("sys");
						record.setUpdateDate(new Date());
						record.setUpdateUser("sys");
						memberTeamRelaMapper.insertSelective(record);
					}

				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		}
	}

}

/*
 * 修改历史 $Log$
 */