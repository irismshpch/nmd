/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月17日
 *******************************************************************************/

package com.hualife.md.team.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hualife.md.team.dao.ex.entity.TeamMembersInfo;
import com.hualife.md.team.dao.mybatis.entity.MemberTeamRela;
import com.hualife.md.team.dao.mybatis.entity.MemberTeamRelaExample;
import com.hualife.md.team.dao.mybatis.entity.TeamInfo;
import com.hualife.md.team.dao.mybatis.mapper.MemberTeamRelaMapper;
import com.hualife.md.team.dao.mybatis.mapper.TeamInfoMapper;
import com.hualife.md.team.service.ITeamManageService;
import com.hualife.support.exception.BusinessException;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
@Service("teamManageService")
public class TeamManageServiceImpl implements ITeamManageService {
	@Autowired
	private TeamInfoMapper teamInfoMapper;
	@Autowired
	private MemberTeamRelaMapper memberTeamRelaMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.team.service.ITeamManageService#teamFoundation(com.hualife
	 * .md.team.dao.mybatis.entity.TeamInfo, java.util.List)
	 */
	@Override
	@Transactional
	public String teamFoundation(TeamInfo teaminfo,
			List<TeamMembersInfo> teamMembersList) throws Exception {
		teamInfoMapper.insertSelective(teaminfo);
		String teamId = teaminfo.getTeamId() + "";
		MemberTeamRela manager = new MemberTeamRela();
		manager.setTeamId(Integer.parseInt(teamId));
		manager.setMemberId(teaminfo.getTeamManager());
		manager.setCreateUser(teaminfo.getCreateUser());
		manager.setCreateDate(new Date());
		manager.setUpdateUser(teaminfo.getCreateUser());
		manager.setUpdateDate(new Date());
		memberTeamRelaMapper.insertSelective(manager);
		for (TeamMembersInfo teamMember : teamMembersList) {

			if (teamMember.getMemberId().equals(teaminfo.getTeamManager())) {
				continue;
			}
			MemberTeamRela member = new MemberTeamRela();
			member.setTeamId(Integer.parseInt(teamId));
			member.setMemberId(teamMember.getMemberId());
			member.setCreateUser(teaminfo.getCreateUser());
			member.setCreateDate(new Date());
			member.setUpdateUser(teaminfo.getCreateUser());
			member.setUpdateDate(new Date());
			memberTeamRelaMapper.insertSelective(member);
		}
		return teamId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.team.service.ITeamManageService#teamChange(com.hualife
	 * .md.team.dao.mybatis.entity.TeamInfo)
	 */
	@Override
	@Transactional
	public void teamChange(TeamInfo teaminfo) throws Exception {
		// 判断用户有无修改权限
		String teamId = teaminfo.getTeamId() + "";
		String memberId = teaminfo.getUpdateUser();
		if(isExists(teamId, memberId)){
			teaminfo.setUpdateDate(new Date());
			teamInfoMapper.updateByPrimaryKeySelective(teaminfo);
		} else {
			throw new BusinessException("用户不具备群信息的修改权限！");
		}
		

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.team.service.ITeamManageService#teamManagerTransition(
	 * com.hualife.md.team.dao.mybatis.entity.TeamInfo)
	 */
	@Override
	@Transactional
	public void teamManagerTransition(TeamInfo teaminfo) throws Exception {
		teaminfo.setUpdateDate(new Date());
		teamInfoMapper.updateByPrimaryKeySelective(teaminfo);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.team.service.ITeamManageService#teamMemberAddition(java
	 * .lang.String, java.lang.String, java.util.List)
	 */
	@Override
	@Transactional
	public void teamMemberAddition(String operator, String teamId,
			List<TeamMembersInfo> teamMembersList) throws Exception {
		for (TeamMembersInfo teamMember : teamMembersList) {
			String memberId = teamMember.getMemberId();
			if(isExists(teamId, memberId)){
				// 存在则直接跳过
			} else{
				MemberTeamRela member = new MemberTeamRela();
				member.setTeamId(Integer.parseInt(teamId));
				member.setMemberId(memberId);
				member.setCreateUser(operator);
				member.setCreateDate(new Date());
				member.setUpdateUser(operator);
				member.setUpdateDate(new Date());
				memberTeamRelaMapper.insertSelective(member);
			}
			
		}
	}

	private boolean isExists(String teamId, String memberId){
		MemberTeamRelaExample example = new MemberTeamRelaExample();
		MemberTeamRelaExample.Criteria criteria = example.createCriteria();
		criteria.andTeamIdEqualTo(Integer.parseInt(teamId));
		criteria.andMemberIdEqualTo(memberId);
		List<MemberTeamRela> result = memberTeamRelaMapper.selectByExample(example);
		if(result != null && result.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hualife.md.team.service.ITeamManageService#teamMemberRemove(java.
	 * lang.String, java.lang.String, java.util.List)
	 */
	@Override
	@Transactional
	public void teamMemberRemove(String operator, String teamId,
			List<TeamMembersInfo> teamMembersList) throws Exception {
		for (TeamMembersInfo teamMember : teamMembersList) {
			MemberTeamRelaExample example = new MemberTeamRelaExample();
			MemberTeamRelaExample.Criteria criteria = example.createCriteria();
			criteria.andTeamIdEqualTo(Integer.parseInt(teamId));
			criteria.andMemberIdEqualTo(teamMember.getMemberId());
			memberTeamRelaMapper.deleteByExample(example);
		}
	}

}

/*
 * 修改历史 $Log$
 */