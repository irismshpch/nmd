/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月09日
 *******************************************************************************/


package com.hualife.md.team.service;

import java.util.List;

import com.hualife.md.team.dao.ex.entity.TeamMembersInfo;
import com.hualife.md.team.dao.ex.entity.TeamQuery;
import com.hualife.md.team.dao.mybatis.entity.TeamInfo;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface ITeamQueryService {
	
	public TeamInfo selectTeamInfo(String teamId);
	
	public List<TeamMembersInfo> selectMembersInfoByTeamId(TeamQuery teamQuery);
	
	public int selectTotalMembersByTeamId(TeamQuery teamQuery);

	public List<TeamMembersInfo> selectTeamsInfoByMemberId(String memberId);
}

/*
 * 修改历史
 * $Log$ 
 */