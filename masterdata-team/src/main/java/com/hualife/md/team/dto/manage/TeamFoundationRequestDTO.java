/**
 * This class is generated by the generator.
 * Any question, please contact to sunxiaodong(13401146791).
 */

package com.hualife.md.team.dto.manage;

import java.util.List;

/**
 * auto generate TeamFoundationRequestDTO model
 * 
 * @author generator
 * @date 2017-10-09
 */
public class TeamFoundationRequestDTO {
	private String teamCreator;
	private String teamName;
	private List<MembersInfoRequestDTO> membersInfo;

	public String getTeamCreator() {
		return teamCreator;
	}

	public void setTeamCreator(String teamCreator) {
		this.teamCreator = teamCreator;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<MembersInfoRequestDTO> getMembersInfo() {
		return membersInfo;
	}

	public void setMembersInfo(List<MembersInfoRequestDTO> membersInfo) {
		this.membersInfo = membersInfo;
	}

}