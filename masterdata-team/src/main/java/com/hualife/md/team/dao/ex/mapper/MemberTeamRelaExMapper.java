package com.hualife.md.team.dao.ex.mapper;

import java.util.List;

import com.hualife.md.team.dao.ex.entity.MemberTeamRelaEx;
import com.hualife.md.team.dao.mybatis.entity.TeamInfo;


public interface MemberTeamRelaExMapper {
    List<MemberTeamRelaEx> selectDiffMemberTeamInfos(TeamInfo teamInfo);
    
    List<String> selectTmsTeamInfos(String groupId);
}