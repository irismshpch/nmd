/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2018年2月6日
 *******************************************************************************/


package com.hualife.md.team.service;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface IWechatUserSynService {
	public void syn2Wechat();
	
	public void syn2MD();
}

/*
 * 修改历史
 * $Log$ 
 */