/**
 * This class is generated by the generator.
 * Any question, please contact to sunxiaodong(13401146791).
 */

package com.hualife.md.team.dto.manage;

/**
 * auto generate MembersInfoResponseDTO model
 * 
 * @author generator
 * @date 2017-10-09
 */
public class MembersInfoResponseDTO {
	private String memberId;
	private String memberName;
	private String memberType;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

}