package com.hualife.md.team.dao.ex.entity;

public class MemberTeamRelaEx {
    private Integer id;
    private Integer teamId;
    private String memberId;
    private String operate;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTeamId() {
        return teamId;
    }

	/**
	 * @return Returns the memberId.
	 */
	public String getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId The memberId to set.
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return Returns the operate.
	 */
	public String getOperate() {
		return operate;
	}

	/**
	 * @param operate The operate to set.
	 */
	public void setOperate(String operate) {
		this.operate = operate;
	}
}