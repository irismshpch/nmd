/*******************************************************************************
 * $Header$
 * $Revision$
 * $Date$
 *
 *==============================================================================
 *
 * Copyright©hxlife/hulife 
 * All rights reserved.
 * 
 * Created on 2017年10月17日
 *******************************************************************************/

package com.hualife.md.team.service;

import java.util.List;

import com.hualife.md.team.dao.ex.entity.TeamMembersInfo;
import com.hualife.md.team.dao.mybatis.entity.TeamInfo;

/**
 * @description: TODO 此处填写 class 信息
 * @author 孙小冬 (mailto:sunxiaodong001@hualife.cc)
 */
public interface ITeamManageService {
	public String teamFoundation(TeamInfo teaminfo, List<TeamMembersInfo> teamMembersList) throws Exception;
	
	public void teamMemberAddition(String operator, String teamId, List<TeamMembersInfo> teamMembersList) throws Exception;
	
	public void teamMemberRemove(String operator, String teamId, List<TeamMembersInfo> teamMembersList) throws Exception;
	
	public void teamChange(TeamInfo teaminfo) throws Exception;
	
	public void teamManagerTransition(TeamInfo teaminfo) throws Exception;
}

/*
 * 修改历史 $Log$
 */